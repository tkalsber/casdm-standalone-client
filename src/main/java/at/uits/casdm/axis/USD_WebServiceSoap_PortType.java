/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * USD_WebServiceSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.uits.casdm.axis;

public interface USD_WebServiceSoap_PortType extends java.rmi.Remote {
    public void createObject(int sid, String objectType, ArrayOfString attrVals, ArrayOfString attributes, javax.xml.rpc.holders.StringHolder createObjectResult, javax.xml.rpc.holders.StringHolder newHandle) throws java.rmi.RemoteException;
    public void addAssetLog(int sid, String assetHandle, String contactHandle, String logText) throws java.rmi.RemoteException;
    public void createLrelRelationships(int sid, String contextObject, String lrelName, ArrayOfString addObjectHandles) throws java.rmi.RemoteException;
    public void addMemberToGroup(int sid, String contactHandle, String groupHandle) throws java.rmi.RemoteException;
    public String attachChangeToRequest(int sid, String creator, String requestHandle, String changeHandle, ArrayOfString changeAttrVals, String description) throws java.rmi.RemoteException;
    public String callServerMethod(int sid, String methodName, String factoryName, String formatList, ArrayOfString parameters) throws java.rmi.RemoteException;
    public String changeStatus(int sid, String creator, String objectHandle, String description, String newStatusHandle) throws java.rmi.RemoteException;
    public int clearNotification(int sid, String lrObject, String clearBy) throws java.rmi.RemoteException;
    public String createActivityLog(int sid, String creator, String objectHandle, String description, String logType, int timeSpent, boolean internal) throws java.rmi.RemoteException;
    public void createAsset(int sid, ArrayOfString attrVals, ArrayOfString attributes, javax.xml.rpc.holders.StringHolder createAssetResult, javax.xml.rpc.holders.StringHolder newAssetHandle, javax.xml.rpc.holders.StringHolder newExtensionHandle, javax.xml.rpc.holders.StringHolder newExtensionName) throws java.rmi.RemoteException;
    public String createAssetParentChildRelationship(int sid, String parentHandle, String childHandle) throws java.rmi.RemoteException;
    public String createAttachment(int sid, String repositoryHandle, String objectHandle, String description, String fileName) throws java.rmi.RemoteException;
    public int removeAttachment(int sid, String attHandle) throws java.rmi.RemoteException;
    public String createChangeOrder(int sid, String creatorHandle, ArrayOfString attrVals, ArrayOfString propertyValues, String template, ArrayOfString attributes, javax.xml.rpc.holders.StringHolder newChangeHandle, javax.xml.rpc.holders.StringHolder newChangeNumber) throws java.rmi.RemoteException;
    public String createIssue(int sid, String creatorHandle, ArrayOfString attrVals, ArrayOfString propertyValues, String template, ArrayOfString attributes, javax.xml.rpc.holders.StringHolder newIssueHandle, javax.xml.rpc.holders.StringHolder newIssueNumber) throws java.rmi.RemoteException;
    public void createWorkFlowTask(int sid, ArrayOfString attrVals, String objectHandle, String creatorHandle, String selectedWorkFlow, String taskType, ArrayOfString attributes, javax.xml.rpc.holders.StringHolder createWorkFlowTaskResult, javax.xml.rpc.holders.StringHolder newHandle) throws java.rmi.RemoteException;
    public void deleteWorkFlowTask(int sid, String workFlowHandle, String objectHandle) throws java.rmi.RemoteException;
    public String createRequest(int sid, String creatorHandle, ArrayOfString attrVals, ArrayOfString propertyValues, String template, ArrayOfString attributes, javax.xml.rpc.holders.StringHolder newRequestHandle, javax.xml.rpc.holders.StringHolder newRequestNumber) throws java.rmi.RemoteException;
    public String createTicket(int sid, String description, String problem_type, String userid, String asset, String duplication_id, javax.xml.rpc.holders.StringHolder newTicketHandle, javax.xml.rpc.holders.StringHolder newTicketNumber, javax.xml.rpc.holders.StringHolder returnUserData, javax.xml.rpc.holders.StringHolder returnApplicationData) throws java.rmi.RemoteException;
    public String createQuickTicket(int sid, String customerHandle, String description, javax.xml.rpc.holders.StringHolder newTicketHandle, javax.xml.rpc.holders.StringHolder newTicketNumber) throws java.rmi.RemoteException;
    public String closeTicket(int sid, String description, String ticketHandle) throws java.rmi.RemoteException;
    public void logComment(int sid, String ticketHandle, String comment, int internalFlag) throws java.rmi.RemoteException;
    public String getPolicyInfo(int sid) throws java.rmi.RemoteException;
    public String detachChangeFromRequest(int sid, String creator, String requestHandle, String description) throws java.rmi.RemoteException;
    public String doSelect(int sid, String objectType, String whereClause, int maxRows, ArrayOfString attributes) throws java.rmi.RemoteException;
    public ListResult doQuery(int sid, String objectType, String whereClause) throws java.rmi.RemoteException;
    public String escalate(int sid, String creator, String objectHandle, String description, boolean setAssignee, String newAssigneeHandle, boolean setGroup, String newGroupHandle, boolean setOrganization, String newOrganizationHandle, boolean setPriority, String newPriorityHandle) throws java.rmi.RemoteException;
    public void freeListHandles(int sid, ArrayOfInt handles) throws java.rmi.RemoteException;
    public void getAssetExtensionInformation(int sid, String assetHandle, ArrayOfString attributes, javax.xml.rpc.holders.StringHolder getAssetExtInfoResult, javax.xml.rpc.holders.StringHolder extensionHandle, javax.xml.rpc.holders.StringHolder extensionName) throws java.rmi.RemoteException;
    public String getConfigurationMode(int sid) throws java.rmi.RemoteException;
    public String getGroupMemberListValues(int sid, String whereClause, int numToFetch, ArrayOfString attributes) throws java.rmi.RemoteException;
    public String getObjectTypeInformation(int sid, String factory) throws java.rmi.RemoteException;
    public String getHandleForUserid(int sid, String userID) throws java.rmi.RemoteException;
    public String getAccessTypeForContact(int sid, String contactHandle) throws java.rmi.RemoteException;
    public String getListValues(int sid, int listHandle, int startIndex, int endIndex, ArrayOfString attributeNames) throws java.rmi.RemoteException;
    public int getLrelLength(int sid, String contextObject, String lrelName) throws java.rmi.RemoteException;
    public String getLrelValues(int sid, String contextObject, String lrelName, int startIndex, int endIndex, ArrayOfString attributes) throws java.rmi.RemoteException;
    public ListResult getNotificationsForContact(int sid, String contactHandle, int queryStatus) throws java.rmi.RemoteException;
    public String getObjectValues(int sid, String objectHandle, ArrayOfString attributes) throws java.rmi.RemoteException;
    public ListResult getPendingChangeTaskListForContact(int sid, String contactHandle) throws java.rmi.RemoteException;
    public ListResult getPendingIssueTaskListForContact(int sid, String contactHandle) throws java.rmi.RemoteException;
    public String getPropertyInfoForCategory(int sid, String categoryHandle, ArrayOfString attributes) throws java.rmi.RemoteException;
    public ListResult getRelatedList(int sid, String objectHandle, String listName) throws java.rmi.RemoteException;
    public void getRelatedListValues(int sid, String objectHandle, String listName, int numToFetch, ArrayOfString attributes, javax.xml.rpc.holders.StringHolder getRelatedListValuesResult, javax.xml.rpc.holders.IntHolder numRowsFound) throws java.rmi.RemoteException;
    public String getWorkFlowTemplates(int sid, String objectHandle, ArrayOfString attributes) throws java.rmi.RemoteException;
    public String getTaskListValues(int sid, String objectHandle, ArrayOfString attributes) throws java.rmi.RemoteException;
    public String getValidTaskTransitions(int sid, String taskHandle, ArrayOfString attributes) throws java.rmi.RemoteException;
    public String getValidTransitions(int sid, String handle, String ticketFactory) throws java.rmi.RemoteException;
    public String getDependentAttrControls(int sid, String handle, ArrayOfString attrVals) throws java.rmi.RemoteException;
    public int login(String username, String password) throws java.rmi.RemoteException;
    public int loginService(String username, String password, String policy) throws java.rmi.RemoteException;
    public String loginServiceManaged(String policy, String encrypted_policy) throws java.rmi.RemoteException;
    public int loginWithArtifact(String userid, String artifact) throws java.rmi.RemoteException;
    public int impersonate(int sid, String userid) throws java.rmi.RemoteException;
    public void logout(int sid) throws java.rmi.RemoteException;
    public String notifyContacts(int sid, String creator, String contextObject, String messageTitle, String messageBody, int notifyLevel, ArrayOfString notifyees, boolean internal) throws java.rmi.RemoteException;
    public void removeLrelRelationships(int sid, String contextObject, String lrelName, ArrayOfString removeObjectHandles) throws java.rmi.RemoteException;
    public void removeMemberFromGroup(int sid, String contactHandle, String groupHandle) throws java.rmi.RemoteException;
    public int serverStatus(int sid) throws java.rmi.RemoteException;
    public String updateObject(int sid, String objectHandle, ArrayOfString attrVals, ArrayOfString attributes) throws java.rmi.RemoteException;
    public String getBopsid(int sid, String contact) throws java.rmi.RemoteException;
    public String getArtifact(int sid, String contact, String passwd) throws java.rmi.RemoteException;
    public String attachURLLinkToTicket(int sid, String ticketHandle, String url, String attmntName, String description) throws java.rmi.RemoteException;
    public String createAttmnt(int sid, String repositoryHandle, int folderId, int objectHandle, String description, String fileName) throws java.rmi.RemoteException;
    public String getDocumentsByIDs(int sid, String docIds, String propertyList, String sortBy, boolean descending) throws java.rmi.RemoteException;
    public String getDecisionTrees(int sid, String propertyList, String sortBy, boolean descending) throws java.rmi.RemoteException;
    public int deleteDocument(int sid, int docId) throws java.rmi.RemoteException;
    public String getCategory(int sid, int catId, boolean getCategoryPaths) throws java.rmi.RemoteException;
    public String getStatuses(int sid) throws java.rmi.RemoteException;
    public String getBookmarks(int sid, String contactId) throws java.rmi.RemoteException;
    public String getQuestionsAsked(int sid, int resultSize, boolean descending) throws java.rmi.RemoteException;
    public String getPriorities(int sid) throws java.rmi.RemoteException;
    public String getDocumentTypes(int sid) throws java.rmi.RemoteException;
    public String getTemplateList(int sid) throws java.rmi.RemoteException;
    public String getWorkflowTemplateList(int sid) throws java.rmi.RemoteException;
    public String addComment(int sid, String comment, int docId, String email, String username, String contactId) throws java.rmi.RemoteException;
    public int deleteComment(int sid, int commentId) throws java.rmi.RemoteException;
    public String createDocument(int sid, ArrayOfString kdAttributes) throws java.rmi.RemoteException;
    public String modifyDocument(int sid, int docId, ArrayOfString kdAttributes) throws java.rmi.RemoteException;
    public String rateDocument(int sid, int docId, int rating, int multiplier, String ticketPerId, boolean onTicketAccept, boolean solveUserProblem, boolean isDefault) throws java.rmi.RemoteException;
    public String getComments(int sid, String docIds) throws java.rmi.RemoteException;
    public String findContacts(int sid, String userName, String lastName, String firstName, String email, String accessType, int inactiveFlag) throws java.rmi.RemoteException;
    public String getPermissionGroups(int sid, int groupId) throws java.rmi.RemoteException;
    public String getContact(int sid, String contactId) throws java.rmi.RemoteException;
    public String addBookmark(int sid, String contactId, int docId) throws java.rmi.RemoteException;
    public String updateRating(int sid, int buId, int rate) throws java.rmi.RemoteException;
    public String doSelectKD(int sid, String whereClause, String sortBy, boolean desc, int maxRows, ArrayOfString attributes, int skip) throws java.rmi.RemoteException;
    public String getFolderList(int sid, int parentFolderId, int repId) throws java.rmi.RemoteException;
    public String getFolderInfo(int sid, int folderId) throws java.rmi.RemoteException;
    public String getAttmntList(int sid, int folderId, int repId) throws java.rmi.RemoteException;
    public String getAttmntInfo(int sid, int attmntId) throws java.rmi.RemoteException;
    public String getRepositoryInfo(int sid, int repositoryId) throws java.rmi.RemoteException;
    public String createFolder(int sid, int parentFolderId, int repId, int folderType, String description, String folderName) throws java.rmi.RemoteException;
    public String faq(int sid, String categoryIds, int resultSize, String propertyList, String sortBy, boolean descending, String whereClause, int maxDocIDs) throws java.rmi.RemoteException;
    public int attmntFolderLinkCount(int sid, int folderId) throws java.rmi.RemoteException;
    public int attachURLLink(int sid, int docId, String url, String attmntName, String description) throws java.rmi.RemoteException;
    public int deleteBookmark(int sid, String contactId, int docId) throws java.rmi.RemoteException;
    public String getKDListPerAttmnt(int sid, int attmntId) throws java.rmi.RemoteException;
    public String getAttmntListPerKD(int sid, int docId) throws java.rmi.RemoteException;
    public int isAttmntLinkedKD(int sid, int attmntId) throws java.rmi.RemoteException;
    public String transfer(int sid, String creator, String objectHandle, String description, boolean setAssignee, String newAssigneeHandle, boolean setGroup, String newGroupHandle, boolean setOrganization, String newOrganizationHandle) throws java.rmi.RemoteException;
    public String search(int sid, String problem, int resultSize, String properties, String sortBy, boolean descending, boolean relatedCategories, int searchType, int matchType, int searchField, String categoryPath, String whereClause, int maxDocIDs) throws java.rmi.RemoteException;
    public String getDocument(int sid, int docId, String propertyList, boolean relatedDoc, boolean getAttmnt, boolean getHistory, boolean getComment, boolean getNotiList) throws java.rmi.RemoteException;
}
