/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * USD_WebServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.uits.casdm.axis;

public class USD_WebServiceLocator extends org.apache.axis.client.Service implements USD_WebService {

    public USD_WebServiceLocator() {
    }


    public USD_WebServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public USD_WebServiceLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for USD_WebServiceSoap
    private String USD_WebServiceSoap_address = "http://tw2k12usd2012s1.integration.uniqa.at:8080/axis/services/USD_R11_WebService";

    public String getUSD_WebServiceSoapAddress() {
        return USD_WebServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private String USD_WebServiceSoapWSDDServiceName = "USD_WebServiceSoap";

    public String getUSD_WebServiceSoapWSDDServiceName() {
        return USD_WebServiceSoapWSDDServiceName;
    }

    public void setUSD_WebServiceSoapWSDDServiceName(String name) {
        USD_WebServiceSoapWSDDServiceName = name;
    }

    public USD_WebServiceSoap_PortType getUSD_WebServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(USD_WebServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getUSD_WebServiceSoap(endpoint);
    }

    public USD_WebServiceSoap_PortType getUSD_WebServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            USD_WebServiceSoapSoapBindingStub _stub = new USD_WebServiceSoapSoapBindingStub(portAddress, this);
            _stub.setPortName(getUSD_WebServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setUSD_WebServiceSoapEndpointAddress(String address) {
        USD_WebServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (USD_WebServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                USD_WebServiceSoapSoapBindingStub _stub = new USD_WebServiceSoapSoapBindingStub(new java.net.URL(USD_WebServiceSoap_address), this);
                _stub.setPortName(getUSD_WebServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("USD_WebServiceSoap".equals(inputPortName)) {
            return getUSD_WebServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.ca.com/UnicenterServicePlus/ServiceDesk", "USD_WebService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.ca.com/UnicenterServicePlus/ServiceDesk", "USD_WebServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {
        
if ("USD_WebServiceSoap".equals(portName)) {
            setUSD_WebServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
