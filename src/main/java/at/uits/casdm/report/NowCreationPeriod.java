/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.report;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Created by VZM9621 on 19.05.2016.
 */
public class NowCreationPeriod extends CreationPeriod {

    public NowCreationPeriod(LocalDateTime from) {
        super(from, LocalDateTime.now(ZoneId.systemDefault()));
    }
}
