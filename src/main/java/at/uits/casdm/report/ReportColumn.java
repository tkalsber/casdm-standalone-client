/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.report;

/**
 * Created by VZM9621 on 19.05.2016.
 */
public enum ReportColumn {
    // attachment_issues
    ID("ID", "id"),
    ISSUE_NR("Issue-Nr", "ref_num"),
    PERSISTENCE_ID("DBID", "persistent_id"),
    INCIDENT_NR("Incident-Nr", "zoriginating_cr.ref_num"),
    DESCRIPTION("Beschreibung", "description"),
    CREATED_DATE("Erzeugt", "open_date"),
    STARTED_DATE("Gestartet", "start_date"),
    CLOSED_DATE("Geschlossen", "close_date"),
    RESOLVED_DATE("Gelöst", "resolve_date"),
    GROUP("Gruppe", "group.last_name"),
    OWNER("Owner", "zowner.name"),
    CATEGORY("Kategorie", "category.sym"),
    SUMMARY("Betreff", "summary"),
    ENDUSER_FIRSTNAME("Ersteller", "log_agent.first_name"),
    ENDUSER_LASTNAME("Ersteller", "log_agent.last_name"),
    ENDUSER_ID("Ersteller", "log_agent.userid"),
    REPORTER_FIRSTNAME("Endanwender", "requestor.first_name"),
    REPORTER_LASTNAME("Endanwender", "requestor.last_name"),
    REPORTER_ID("Endanwender", "requestor.userid"),
    PHONE_NUMBER("Telefon", "requestor.phone_number"),
    MOBILE_NUMBER("Mobiltelefon", "requestor.beeper_phone"),
    STATUS_ID("Status-ID", "status.id"),
    STATUS_NAME("Status-Name", "status.sym"),
    PRIORITY("Priorität", "priority.sym"),
    ASSIGNEE_FIRSTNAME("Bearbeiter", "assignee.first_name"),
    ASSIGNEE_LASTNAME("Bearbeiter", "assignee.last_name"),
    ASSIGNEE_ID("Bearbeiter", "assignee.userid");

    private String displayName;
    private String internalName;

    ReportColumn(String displayName, String internalName) {
        this.displayName = displayName;
        this.internalName = internalName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getInternalName() {
        return internalName;
    }
}
