
package at.uits.casdm.pojo;

import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}Attribute" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "attribute"
})
@XmlRootElement(name = "Attributes")
public class Attributes {

    @XmlElement(name = "Attribute", required = true)
    protected List<Attribute> attribute;

    /**
     * Gets the value of the attribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Attribute }
     * 
     * 
     */
    public List<Attribute> getAttribute() {
        if (attribute == null) {
            attribute = new ArrayList<Attribute>();
        }
        return this.attribute;
    }

    /**
     * Get attribute by name.
     *
     * @param attributeName the name of the attribute
     * @return a attribute with the specified name on success, else null.
     */
    public Attribute getAttributeByName(String attributeName) {
        if(StringUtils.isBlank(attributeName))
            return null;
        List<Attribute> attributes = getAttribute();
        for(Attribute attribute : attributes)
            if(attributeName.equalsIgnoreCase(attribute.getAttrName()))
                return attribute;
        return null;
    }

    /**
     * Check if attributes contains a named attribute.
     *
     * @param attributeName the attribute to search for
     * @return true on success, else false.
     */
    public boolean containsAttribute(String attributeName) {
        if(StringUtils.isBlank(attributeName))
            return false;
        List<Attribute> attributes = getAttribute();
        for(Attribute attribute : attributes)
            if(attributeName.equalsIgnoreCase(attribute.getAttrName()))
                return true;
        return false;
    }

}
