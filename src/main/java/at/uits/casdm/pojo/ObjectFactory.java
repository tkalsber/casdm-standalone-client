
package at.uits.casdm.pojo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.uits.casdm.pojo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AttrName_QNAME = new QName("", "AttrName");
    private final static QName _AttrValue_QNAME = new QName("", "AttrValue");
    private final static QName _Handle_QNAME = new QName("", "Handle");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.uits.casdm.pojo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UDSObject }
     * 
     */
    public UDSObject createUDSObject() {
        return new UDSObject();
    }

    /**
     * Create an instance of {@link Attributes }
     * 
     */
    public Attributes createAttributes() {
        return new Attributes();
    }

    /**
     * Create an instance of {@link Attribute }
     * 
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link UDSObjectList }
     * 
     */
    public UDSObjectList createUDSObjectList() {
        return new UDSObjectList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AttrName")
    public JAXBElement<String> createAttrName(String value) {
        return new JAXBElement<String>(_AttrName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AttrValue")
    public JAXBElement<String> createAttrValue(String value) {
        return new JAXBElement<String>(_AttrValue_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Handle")
    public JAXBElement<String> createHandle(String value) {
        return new JAXBElement<String>(_Handle_QNAME, String.class, null, value);
    }

}
