/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.pojo;

import java.io.Serializable;

/**
 * Created by VZM9621 on 20.05.2016.
 */
public class IssueProperty implements Serializable {

    private String description;
    private String name;
    private String value;

    public IssueProperty(String name, String value, String description) {
        this.description = description;
        this.name = name;
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return (value == null) ? "" : value;
    }

    public void setValue(String value) {
        if(value == null) this.value = "";
        else this.value = value;
    }


    @Override
    public String toString() {
        return "\nIssueProperty [ \n" +
                "description : " + description + "\n" +
                "name : " + name + "\n" +
                "value : " + value + "\n" +
                " ] \n";
    }
}
