/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.pojo;

import at.uits.casdm.report.ReportColumn;
import com.google.common.collect.Lists;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by VZM9621 on 20.05.2016.
 */
public class IssueFactory {

    public static List<Issue> getIssuesFromUDSObjectList(UDSObjectList objList) {
        List<Issue> issues = Lists.newArrayList();
        for (UDSObject obj : objList.getUDSObject()) {
            issues.add(getIssueFromUDSObject(obj));
        }
        return issues;
    }

    public static List<Attachment> getAttachmentsFromUDSObjectList(UDSObjectList objList) {
        List<Attachment> attachments = Lists.newArrayList();
        for (UDSObject obj : objList.getUDSObject()) {
            attachments.add(getAttachmentFromUDSObject(obj));
        }
        return attachments;
    }

    private static Attachment getAttachmentFromUDSObject(UDSObject obj) {
        Attachment attachment = new Attachment();
        Attribute attribute = null;

        attribute = obj.getAttributeByName("id");
        attachment.setId(attribute.getAttrValue());

        attribute = obj.getAttributeByName("attmnt_name");
        attachment.setAttachmentName(attribute.getAttrValue());

        attribute = obj.getAttributeByName("repository");
        attachment.setRepository(attribute.getAttrValue());

        attribute = obj.getAttributeByName("file_type");
        attachment.setMimeType(attribute.getAttrValue());

        attribute = obj.getAttributeByName("file_size");
        attachment.setFileSize(Long.parseLong(attribute.getAttrValue()));

        attribute = obj.getAttributeByName("file_name");
        attachment.setFileName(attribute.getAttrValue());

        attribute = obj.getAttributeByName("orig_file_name");
        attachment.setOrigFileName(attribute.getAttrValue());

        attribute = obj.getAttributeByName("persistent_id");
        attachment.setPersistenceId(attribute.getAttrValue());

        attribute = obj.getAttributeByName("repository");
        attachment.setRepository(attribute.getAttrValue());

        return attachment;
    }

    public static Issue getIssueFromUDSObject(UDSObject obj) {
        Issue issue = new Issue();
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        Attribute attribute = null;

        attribute = obj.getAttributeByName(ReportColumn.ASSIGNEE_FIRSTNAME.getInternalName());
        issue.setAssigneeFirstname(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.ASSIGNEE_LASTNAME.getInternalName());
        issue.setAssigneeLastname(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.ASSIGNEE_ID.getInternalName());
        issue.setAssigneeId(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.CATEGORY.getInternalName());
        issue.setCategory(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.CLOSED_DATE.getInternalName());
        issue.setClosedDate(df.format(new Date(Long.parseLong(attribute.getAttrValue()+"000"))));

        attribute = obj.getAttributeByName(ReportColumn.CREATED_DATE.getInternalName());
        issue.setCreatedDate(df.format(new Date(Long.parseLong(attribute.getAttrValue()+"000"))));

        attribute = obj.getAttributeByName(ReportColumn.DESCRIPTION.getInternalName());
        issue.setDescription(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.ENDUSER_FIRSTNAME.getInternalName());
        issue.setEnduserFirstname(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.ENDUSER_LASTNAME.getInternalName());
        issue.setEnduserLastname(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.ENDUSER_ID.getInternalName());
        issue.setEnduserId(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.GROUP.getInternalName());
        issue.setGroup(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.ID.getInternalName());
        issue.setId(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.INCIDENT_NR.getInternalName());
        issue.setIncidentNr(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.ISSUE_NR.getInternalName());
        issue.setIssueNr(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.OWNER.getInternalName());
        issue.setOwner(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.PERSISTENCE_ID.getInternalName());
        issue.setPersistenceId(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.PRIORITY.getInternalName());
        issue.setPriority(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.RESOLVED_DATE.getInternalName());
        issue.setResolvedDate(df.format(new Date(Long.parseLong(attribute.getAttrValue()+"000"))));

        attribute = obj.getAttributeByName(ReportColumn.REPORTER_FIRSTNAME.getInternalName());
        issue.setReporterFirstname(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.REPORTER_LASTNAME.getInternalName());
        issue.setReporterLastname(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.REPORTER_ID.getInternalName());
        issue.setReporterId(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.STARTED_DATE.getInternalName());
        issue.setStartedDate(df.format(new Date(Long.parseLong(attribute.getAttrValue()+"000"))));

        attribute = obj.getAttributeByName(ReportColumn.STATUS_ID.getInternalName());
        issue.setStatusId(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.STATUS_NAME.getInternalName());
        issue.setStatusName(attribute.getAttrValue());

        attribute = obj.getAttributeByName(ReportColumn.SUMMARY.getInternalName());
        issue.setSummary(attribute.getAttrValue());

        return issue;
    }
}
