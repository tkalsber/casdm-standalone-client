
package at.uits.casdm.pojo.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}link"/>
 *         &lt;element ref="{}access_key"/>
 *         &lt;element ref="{}expiration_date"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;enumeration value="400020"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="REL_ATTR" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;enumeration value="400020"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="COMMON_NAME" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;enumeration value="64791358"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "link",
    "accessKey",
    "expirationDate"
})
@XmlRootElement(name = "rest_access")
public class RestAccess {

    @XmlElement(required = true)
    protected Link link;
    @XmlElement(name = "access_key")
    protected int accessKey;
    @XmlElement(name = "expiration_date")
    protected int expirationDate;
    @XmlAttribute(name = "id", required = true)
    protected int id;
    @XmlAttribute(name = "REL_ATTR", required = true)
    protected int relattr;
    @XmlAttribute(name = "COMMON_NAME", required = true)
    protected int commonname;

    /**
     * Ruft den Wert der link-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Link }
     *     
     */
    public Link getLink() {
        return link;
    }

    /**
     * Legt den Wert der link-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Link }
     *     
     */
    public void setLink(Link value) {
        this.link = value;
    }

    /**
     * Ruft den Wert der accessKey-Eigenschaft ab.
     * 
     */
    public int getAccessKey() {
        return accessKey;
    }

    /**
     * Legt den Wert der accessKey-Eigenschaft fest.
     * 
     */
    public void setAccessKey(int value) {
        this.accessKey = value;
    }

    /**
     * Ruft den Wert der expirationDate-Eigenschaft ab.
     * 
     */
    public int getExpirationDate() {
        return expirationDate;
    }

    /**
     * Legt den Wert der expirationDate-Eigenschaft fest.
     * 
     */
    public void setExpirationDate(int value) {
        this.expirationDate = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der relattr-Eigenschaft ab.
     * 
     */
    public int getRELATTR() {
        return relattr;
    }

    /**
     * Legt den Wert der relattr-Eigenschaft fest.
     * 
     */
    public void setRELATTR(int value) {
        this.relattr = value;
    }

    /**
     * Ruft den Wert der commonname-Eigenschaft ab.
     * 
     */
    public int getCOMMONNAME() {
        return commonname;
    }

    /**
     * Legt den Wert der commonname-Eigenschaft fest.
     * 
     */
    public void setCOMMONNAME(int value) {
        this.commonname = value;
    }

}
