
package at.uits.casdm.pojo.rest;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.uits.casdm.pojo.rest package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class RestObjectFactory {

    private final static QName _AccessKey_QNAME = new QName("", "access_key");
    private final static QName _ExpirationDate_QNAME = new QName("", "expiration_date");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.uits.casdm.pojo.rest
     * 
     */
    public RestObjectFactory() {
    }

    /**
     * Create an instance of {@link RestAccess }
     * 
     */
    public RestAccess createRestAccess() {
        return new RestAccess();
    }

    /**
     * Create an instance of {@link Link }
     * 
     */
    public Link createLink() {
        return new Link();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "access_key")
    public JAXBElement<Integer> createAccessKey(Integer value) {
        return new JAXBElement<Integer>(_AccessKey_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "expiration_date")
    public JAXBElement<Integer> createExpirationDate(Integer value) {
        return new JAXBElement<Integer>(_ExpirationDate_QNAME, Integer.class, null, value);
    }

}
