/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.pojo;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

/**
 * Created by VZM9621 on 20.05.2016.
 */
public class Issue implements Serializable {

    private String id;
    private String issueNr;
    private String incidentNr;
    private String group;
    private String owner;
    private String description;
    private String summary;
    private String enduserFirstname;
    private String enduserLastname;
    private String enduserId;
    private String assigneeFirstname;
    private String assigneeLastname;
    private String assigneeId;
    private String reporterFirstname;
    private String reporterLastname;
    private String reporterId;
    private String statusId;
    private String statusName;
    private String category;
    private String priority;
    private String persistenceId;
    private String createdDate;
    private String startedDate;
    private String resolvedDate;
    private String closedDate;

    private List<IssueProperty> properties = Lists.newArrayList();
    private List<Attachment> attachments = Lists.newArrayList();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIssueNr() {
        return issueNr;
    }

    public void setIssueNr(String issueNr) {
        this.issueNr = issueNr;
    }

    public String getIncidentNr() {
        return incidentNr;
    }

    public void setIncidentNr(String incidentNr) {
        this.incidentNr = incidentNr;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getEnduserFirstname() {
        return enduserFirstname;
    }

    public void setEnduserFirstname(String enduserFirstname) {
        this.enduserFirstname = enduserFirstname;
    }

    public String getEnduserLastname() {
        return enduserLastname;
    }

    public void setEnduserLastname(String enduserLastname) {
        this.enduserLastname = enduserLastname;
    }

    public String getAssigneeFirstname() {
        return assigneeFirstname;
    }

    public void setAssigneeFirstname(String assigneeFirstname) {
        this.assigneeFirstname = assigneeFirstname;
    }

    public String getAssigneeLastname() {
        return assigneeLastname;
    }

    public void setAssigneeLastname(String assigneeLastname) {
        this.assigneeLastname = assigneeLastname;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPersistenceId() {
        return persistenceId;
    }

    public void setPersistenceId(String persistenceId) {
        this.persistenceId = persistenceId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(String startedDate) {
        this.startedDate = startedDate;
    }

    public String getResolvedDate() {
        return resolvedDate;
    }

    public void setResolvedDate(String resolvedDate) {
        this.resolvedDate = resolvedDate;
    }

    public String getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(String closedDate) {
        this.closedDate = closedDate;
    }

    public String getReporterFirstname() {
        return reporterFirstname;
    }

    public void setReporterFirstname(String reporterFirstname) {
        this.reporterFirstname = reporterFirstname;
    }

    public String getReporterLastname() {
        return reporterLastname;
    }

    public void setReporterLastname(String reporterLastname) {
        this.reporterLastname = reporterLastname;
    }

    public String getReporterId() {
        return reporterId;
    }

    public void setReporterId(String reporterId) {
        this.reporterId = reporterId;
    }
    public String getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(String assigneeId) {
        this.assigneeId = assigneeId;
    }

    public String getEnduserId() {
        return enduserId;
    }

    public void setEnduserId(String enduserId) {
        this.enduserId = enduserId;
    }

    public List<IssueProperty> getProperties() {
        return properties;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    @Override
    public String toString() {
        return "Issue [ \n" +
                "assigneeFirstname : " + assigneeFirstname + "\n" +
                "assigneeLastname : " + assigneeLastname + "\n" +
                "category : " + category + "\n" +
                "closedDate : " + closedDate + "\n" +
                "createdDate : " + createdDate + "\n" +
                "description : " + description + "\n" +
                "enduserFirstname : " + enduserFirstname + "\n" +
                "enduserLastname : " + enduserLastname + "\n" +
                "group : " + group + "\n" +
                "id : " + id + "\n" +
                "incidentNr : " + incidentNr + "\n" +
                "issueNr : " + issueNr + "\n" +
                "owner : " + owner + "\n" +
                "persistenceId : " + persistenceId + "\n" +
                "priority : " + priority + "\n" +
                "reporterFirstname : " + reporterFirstname + "\n" +
                "reporterLastname : " + reporterLastname + "\n" +
                "resolvedDate : " + resolvedDate + "\n" +
                "startedDate : " + startedDate + "\n" +
                "statusId : " + statusId + "\n" +
                "statusName : " + statusName + "\n" +
                "summary : " + summary + "\n" +
                "issueProperties : " + properties + "\n" +
                "attachments : " + attachments + "\n" +
                " ] \n";
    }
}
