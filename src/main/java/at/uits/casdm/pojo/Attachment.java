/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.pojo;

/**
 * Created by VZM9621 on 23.05.2016.
 */
public class Attachment {

    private String repository; // : doc_rep:130000003
    private String id;
    private String mimeType;
    private long fileSize;
    private String fileName;
    private String origFileName;
    private String attachmentName;
    private String persistenceId;

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOrigFileName() {
        return origFileName;
    }

    public void setOrigFileName(String origFileName) {
        this.origFileName = origFileName;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public String getPersistenceId() {
        return persistenceId;
    }

    public void setPersistenceId(String persistenceId) {
        this.persistenceId = persistenceId;
    }

    @Override
    public String toString() {
        return "Attachment [ \n" +
                "attachmentName : " + attachmentName + "\n" +
                "fileName : " + fileName + "\n" +
                "fileSize : " + fileSize + "\n" +
                "id : " + id + "\n" +
                "mimeType : " + mimeType + "\n" +
                "origFileName : " + origFileName + "\n" +
                "persistenceId : " + persistenceId + "\n" +
                "repository : " + repository + "\n" +
                " ] \n";
    }
}
