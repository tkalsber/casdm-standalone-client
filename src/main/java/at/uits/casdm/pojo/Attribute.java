
package at.uits.casdm.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}AttrName"/>
 *         &lt;element ref="{}AttrValue"/>
 *       &lt;/sequence>
 *       &lt;attribute name="DataType" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
 *             &lt;enumeration value="2001"/>
 *             &lt;enumeration value="2002"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "attrName",
    "attrValue"
})
@XmlRootElement(name = "Attribute")
public class Attribute {

    @XmlElement(name = "AttrName", required = true)
    protected String attrName;
    @XmlElement(name = "AttrValue", required = true)
    protected String attrValue;
    @XmlAttribute(name = "DataType", required = true)
    protected short dataType;

    /**
     * Ruft den Wert der attrName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrName() {
        return attrName;
    }

    /**
     * Legt den Wert der attrName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrName(String value) {
        this.attrName = value;
    }

    /**
     * Ruft den Wert der attrValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrValue() {
        return (attrValue != null) ? attrValue : "";
    }

    /**
     * Legt den Wert der attrValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrValue(String value) {
        this.attrValue = value;
    }

    /**
     * Ruft den Wert der dataType-Eigenschaft ab.
     * 
     */
    public short getDataType() {
        return dataType;
    }

    /**
     * Legt den Wert der dataType-Eigenschaft fest.
     * 
     */
    public void setDataType(short value) {
        this.dataType = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Attribute)) return false;

        Attribute attribute = (Attribute) o;

        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(attrName, attribute.attrName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(attrName)
                .toHashCode();
    }

    @Override
    public String toString() {
        return getAttrName() + " : " + getAttrValue();
    }
}
