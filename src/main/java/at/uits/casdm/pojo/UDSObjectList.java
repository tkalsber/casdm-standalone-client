
package at.uits.casdm.pojo;

import com.google.common.collect.Lists;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}UDSObject" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "udsObject"
})
@XmlRootElement(name = "UDSObjectList")
public class UDSObjectList {

    @XmlElement(name = "UDSObject", required = true)
    protected List<UDSObject> udsObject;

    /**
     * Gets the value of the udsObject property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the udsObject property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUDSObject().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UDSObject }
     * 
     * 
     */
    public List<UDSObject> getUDSObject() {
        if (udsObject == null) {
            udsObject = new ArrayList<UDSObject>();
        }
        return this.udsObject;
    }

    /**
     * Get all attributes matching the specified attributename.
     *
     * @param attributeName the attributename
     * @return a list holding zero or more attributes.
     */
    public List<Attribute> getAttributesByName(String attributeName) {
        List<UDSObject> udsObjectList = getUDSObject();
        List<Attribute> attributes = Lists.newArrayList();
        Attribute attribute = null;
        for (UDSObject udsObject : udsObjectList) {
            attribute = udsObject.getAttributeByName(attributeName);
            if(attribute != null) attributes.add(attribute);
        }
        return attributes;
    }

    /**
     *
     * @param attributeName
     * @return
     */
    public List<String> getAttributeValuesByName(String attributeName) {
        List<UDSObject> udsObjectList = getUDSObject();
        List<String> attributeValues = Lists.newArrayList();
        Attribute attribute = null;
        for (UDSObject udsObject : udsObjectList) {
            attribute = udsObject.getAttributeByName(attributeName);
            if(attribute != null) attributeValues.add(attribute.getAttrValue());
        }
        return attributeValues;
    }
}
