
package at.uits.casdm.rest.pojo.category;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}link"/>
 *         &lt;element ref="{}sym"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;enumeration value="120000000"/>
 *             &lt;enumeration value="130000001"/>
 *             &lt;enumeration value="130000002"/>
 *             &lt;enumeration value="130000003"/>
 *             &lt;enumeration value="130000004"/>
 *             &lt;enumeration value="130000005"/>
 *             &lt;enumeration value="130000006"/>
 *             &lt;enumeration value="130000007"/>
 *             &lt;enumeration value="130000008"/>
 *             &lt;enumeration value="130000009"/>
 *             &lt;enumeration value="130000010"/>
 *             &lt;enumeration value="130000011"/>
 *             &lt;enumeration value="130000012"/>
 *             &lt;enumeration value="130000013"/>
 *             &lt;enumeration value="130000014"/>
 *             &lt;enumeration value="130000015"/>
 *             &lt;enumeration value="130000016"/>
 *             &lt;enumeration value="130000017"/>
 *             &lt;enumeration value="130000018"/>
 *             &lt;enumeration value="130000019"/>
 *             &lt;enumeration value="130000020"/>
 *             &lt;enumeration value="159101"/>
 *             &lt;enumeration value="400003"/>
 *             &lt;enumeration value="9100"/>
 *             &lt;enumeration value="9101"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="REL_ATTR" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="HW1"/>
 *             &lt;enumeration value="RIT_HW_1"/>
 *             &lt;enumeration value="RIT_HW_2"/>
 *             &lt;enumeration value="RIT_HW_3"/>
 *             &lt;enumeration value="RIT_HW_4"/>
 *             &lt;enumeration value="RIT_NET_1"/>
 *             &lt;enumeration value="RIT_NET_2"/>
 *             &lt;enumeration value="RIT_NET_3"/>
 *             &lt;enumeration value="RIT_NET_4"/>
 *             &lt;enumeration value="ST_HW_CL_ACC"/>
 *             &lt;enumeration value="ST_HW_PR_ACC"/>
 *             &lt;enumeration value="ST_HW_PR_MAT"/>
 *             &lt;enumeration value="ST_NET"/>
 *             &lt;enumeration value="ST_SVR_VOR"/>
 *             &lt;enumeration value="SW1"/>
 *             &lt;enumeration value="SW_INAS_FL"/>
 *             &lt;enumeration value="SW_INAS_SKV"/>
 *             &lt;enumeration value="SW_INAS_SLO"/>
 *             &lt;enumeration value="SW_INAS_UCZ"/>
 *             &lt;enumeration value="SW_INAS_ULI"/>
 *             &lt;enumeration value="SW_INAS_USK"/>
 *             &lt;enumeration value="SW_INAS_USV"/>
 *             &lt;enumeration value="_AUTOTASKS_"/>
 *             &lt;enumeration value="sd2sd.test"/>
 *             &lt;enumeration value="z120000000"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="COMMON_NAME" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="Hardware.Client.Zubeh�r"/>
 *             &lt;enumeration value="Hardware.Drucker.Verbrauchsmaterial"/>
 *             &lt;enumeration value="Hardware.Drucker.Zubeh�r"/>
 *             &lt;enumeration value="Hardware.Fax"/>
 *             &lt;enumeration value="Hardware.Server"/>
 *             &lt;enumeration value="Hardware.Server.AS400-iSeries"/>
 *             &lt;enumeration value="Hardware.Telefonie-Festnetz"/>
 *             &lt;enumeration value="Netzwerk.Internet.Browser"/>
 *             &lt;enumeration value="Netzwerk.LAN"/>
 *             &lt;enumeration value="Netzwerk.VorOrt"/>
 *             &lt;enumeration value="SD Test"/>
 *             &lt;enumeration value="Software.PA2.INAS_ULI"/>
 *             &lt;enumeration value="Software.PA2.INAS_USV"/>
 *             &lt;enumeration value="__Issue_Bausteine"/>
 *             &lt;enumeration value="inact_Hardware.pc.install"/>
 *             &lt;enumeration value="inact_Netzwerk.Client.share"/>
 *             &lt;enumeration value="inact_Netzwerk.Internet.ELBA"/>
 *             &lt;enumeration value="inact_Server.VorOrt"/>
 *             &lt;enumeration value="inact_Software.INAS_FL"/>
 *             &lt;enumeration value="inact_Software.INAS_SKV"/>
 *             &lt;enumeration value="inact_Software.INAS_SLO"/>
 *             &lt;enumeration value="inact_Software.INAS_UCZ"/>
 *             &lt;enumeration value="inact_Software.INAS_USK"/>
 *             &lt;enumeration value="inact_Software.pc.install"/>
 *             &lt;enumeration value="z120000000"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "link",
    "sym"
})
@XmlRootElement(name = "isscat")
public class Isscat {

    @XmlElement(required = true)
    protected Link link;
    @XmlElement(required = true)
    protected String sym;
    @XmlAttribute(name = "id", required = true)
    protected int id;
    @XmlAttribute(name = "REL_ATTR", required = true)
    protected String relattr;
    @XmlAttribute(name = "COMMON_NAME", required = true)
    protected String commonname;

    /**
     * Ruft den Wert der link-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Link }
     *     
     */
    public Link getLink() {
        return link;
    }

    /**
     * Legt den Wert der link-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Link }
     *     
     */
    public void setLink(Link value) {
        this.link = value;
    }

    /**
     * Ruft den Wert der sym-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSym() {
        return sym;
    }

    /**
     * Legt den Wert der sym-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSym(String value) {
        this.sym = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der relattr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRELATTR() {
        return relattr;
    }

    /**
     * Legt den Wert der relattr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRELATTR(String value) {
        this.relattr = value;
    }

    /**
     * Ruft den Wert der commonname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMMONNAME() {
        return commonname;
    }

    /**
     * Legt den Wert der commonname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMMONNAME(String value) {
        this.commonname = value;
    }

}
