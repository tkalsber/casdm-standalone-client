
package at.uits.casdm.rest.pojo.issue;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}link"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="U'0EE0E7C2CCB9F34DADFF72D882979F1E'"/>
 *             &lt;enumeration value="U'1D7AE3FA5BB6F942BC2D170A5CDA7B8F'"/>
 *             &lt;enumeration value="U'3099BF9BC3DC7048B27681A32FE30026'"/>
 *             &lt;enumeration value="U'320A5DF777DE4549A0855A37ACC74330'"/>
 *             &lt;enumeration value="U'40C599D1AAACF54CA55253EDA94B3F22'"/>
 *             &lt;enumeration value="U'4D061CC8BE0E474B8A64AFB2F2B9B84B'"/>
 *             &lt;enumeration value="U'57EBC0661DD19648A420848DD7D67AAC'"/>
 *             &lt;enumeration value="U'6A1C41E19803B943BDD2D46EF79AB761'"/>
 *             &lt;enumeration value="U'752C4914DCDDD64998432B904921E56A'"/>
 *             &lt;enumeration value="U'86F4E4BDC68416468EBB470EAE9369D3'"/>
 *             &lt;enumeration value="U'A28F45DBB259024D8D8BB3BB839A5A5A'"/>
 *             &lt;enumeration value="U'A91F3EF11E42A340B1ADCC1C4AFC2DD4'"/>
 *             &lt;enumeration value="U'AF6ED441CEFAA142990FCE92A4341BBA'"/>
 *             &lt;enumeration value="U'BC43F840AA00554E97B1098E7448283C'"/>
 *             &lt;enumeration value="U'BC6F7FAF5AE4C24999A1FB8BAFF363ED'"/>
 *             &lt;enumeration value="U'C775FF3606C2A746A2278D239DDA5C19'"/>
 *             &lt;enumeration value="U'CB22DAF93DE050469C5968E8EF05AB7A'"/>
 *             &lt;enumeration value="U'DF0037DBDFCF42499AD23C8DA432FAF0'"/>
 *             &lt;enumeration value="U'DF9DF17F9FCC8E4CA64F85E8440537F9'"/>
 *             &lt;enumeration value="U'DFB2D1F5C1D6ED4F975B2933556C437A'"/>
 *             &lt;enumeration value="U'F5E6480CBFF6F44D8B575A99A67830E2'"/>
 *             &lt;enumeration value="U'FF61FD9193A2294D8512CE8C1C494715'"/>
 *             &lt;enumeration value="U'FFFBCA49E3674644BCE89B69EE687650'"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="REL_ATTR" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="U'0EE0E7C2CCB9F34DADFF72D882979F1E'"/>
 *             &lt;enumeration value="U'1D7AE3FA5BB6F942BC2D170A5CDA7B8F'"/>
 *             &lt;enumeration value="U'3099BF9BC3DC7048B27681A32FE30026'"/>
 *             &lt;enumeration value="U'320A5DF777DE4549A0855A37ACC74330'"/>
 *             &lt;enumeration value="U'40C599D1AAACF54CA55253EDA94B3F22'"/>
 *             &lt;enumeration value="U'4D061CC8BE0E474B8A64AFB2F2B9B84B'"/>
 *             &lt;enumeration value="U'57EBC0661DD19648A420848DD7D67AAC'"/>
 *             &lt;enumeration value="U'6A1C41E19803B943BDD2D46EF79AB761'"/>
 *             &lt;enumeration value="U'752C4914DCDDD64998432B904921E56A'"/>
 *             &lt;enumeration value="U'86F4E4BDC68416468EBB470EAE9369D3'"/>
 *             &lt;enumeration value="U'A28F45DBB259024D8D8BB3BB839A5A5A'"/>
 *             &lt;enumeration value="U'A91F3EF11E42A340B1ADCC1C4AFC2DD4'"/>
 *             &lt;enumeration value="U'AF6ED441CEFAA142990FCE92A4341BBA'"/>
 *             &lt;enumeration value="U'BC43F840AA00554E97B1098E7448283C'"/>
 *             &lt;enumeration value="U'BC6F7FAF5AE4C24999A1FB8BAFF363ED'"/>
 *             &lt;enumeration value="U'C775FF3606C2A746A2278D239DDA5C19'"/>
 *             &lt;enumeration value="U'CB22DAF93DE050469C5968E8EF05AB7A'"/>
 *             &lt;enumeration value="U'DF0037DBDFCF42499AD23C8DA432FAF0'"/>
 *             &lt;enumeration value="U'DF9DF17F9FCC8E4CA64F85E8440537F9'"/>
 *             &lt;enumeration value="U'DFB2D1F5C1D6ED4F975B2933556C437A'"/>
 *             &lt;enumeration value="U'F5E6480CBFF6F44D8B575A99A67830E2'"/>
 *             &lt;enumeration value="U'FF61FD9193A2294D8512CE8C1C494715'"/>
 *             &lt;enumeration value="U'FFFBCA49E3674644BCE89B69EE687650'"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="COMMON_NAME" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="AICHINGER, Alfred  "/>
 *             &lt;enumeration value="ALBRECHT, Sabine  "/>
 *             &lt;enumeration value="AMBROSCH, Manuel  "/>
 *             &lt;enumeration value="BIRKNER, Martin Mag."/>
 *             &lt;enumeration value="DAMBROS-CANZIN, Siegfried Bez.Disp."/>
 *             &lt;enumeration value="ECKELHART, Eva-Maria  "/>
 *             &lt;enumeration value="EDLMANN, Patric  "/>
 *             &lt;enumeration value="FL, Reader "/>
 *             &lt;enumeration value="FLANDORFER, Christine  "/>
 *             &lt;enumeration value="LANG(R-IT), Wolfgang "/>
 *             &lt;enumeration value="LENZINGER, Andreas Bez.Disp."/>
 *             &lt;enumeration value="LERPERGER, Ines Repr."/>
 *             &lt;enumeration value="MAYR, Martin  "/>
 *             &lt;enumeration value="MOESTL, Wolfgang  "/>
 *             &lt;enumeration value="MOIK, Claudia  "/>
 *             &lt;enumeration value="MOSSER-PERCHENIG, Nina  "/>
 *             &lt;enumeration value="PFALLER, Andrea Repr."/>
 *             &lt;enumeration value="SCHMID, Adolf Ob.Sekr."/>
 *             &lt;enumeration value="SKUMAUTZ, Christine  "/>
 *             &lt;enumeration value="STEIDLE, Barbara Mag."/>
 *             &lt;enumeration value="STEINBOECK, Ulrika  "/>
 *             &lt;enumeration value="TSCHINDER, Thomas  "/>
 *             &lt;enumeration value="WIESNER, Karin Sekr."/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "link"
})
@XmlRootElement(name = "requestor")
public class Requestor {

    @XmlElement(required = true)
    protected Link link;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "REL_ATTR", required = true)
    protected String relattr;
    @XmlAttribute(name = "COMMON_NAME", required = true)
    protected String commonname;

    /**
     * Ruft den Wert der link-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Link }
     *     
     */
    public Link getLink() {
        return link;
    }

    /**
     * Legt den Wert der link-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Link }
     *     
     */
    public void setLink(Link value) {
        this.link = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der relattr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRELATTR() {
        return relattr;
    }

    /**
     * Legt den Wert der relattr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRELATTR(String value) {
        this.relattr = value;
    }

    /**
     * Ruft den Wert der commonname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMMONNAME() {
        return commonname;
    }

    /**
     * Legt den Wert der commonname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMMONNAME(String value) {
        this.commonname = value;
    }

}
