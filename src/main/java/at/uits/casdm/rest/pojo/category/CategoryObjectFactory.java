
package at.uits.casdm.rest.pojo.category;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.uits.casdm.rest.pojo.category package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class CategoryObjectFactory {

    private final static QName _Sym_QNAME = new QName("", "sym");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.uits.casdm.rest.pojo.category
     * 
     */
    public CategoryObjectFactory() {
    }

    /**
     * Create an instance of {@link CollectionIsscat }
     * 
     */
    public CollectionIsscat createCollectionIsscat() {
        return new CollectionIsscat();
    }

    /**
     * Create an instance of {@link Link }
     * 
     */
    public Link createLink() {
        return new Link();
    }

    /**
     * Create an instance of {@link Isscat }
     * 
     */
    public Isscat createIsscat() {
        return new Isscat();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sym")
    public JAXBElement<String> createSym(String value) {
        return new JAXBElement<String>(_Sym_QNAME, String.class, null, value);
    }

}
