
package at.uits.casdm.rest.pojo.issue;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}link"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;enumeration value="130000026"/>
 *             &lt;enumeration value="130000053"/>
 *             &lt;enumeration value="130000075"/>
 *             &lt;enumeration value="130000081"/>
 *             &lt;enumeration value="130000225"/>
 *             &lt;enumeration value="130000248"/>
 *             &lt;enumeration value="200000031"/>
 *             &lt;enumeration value="220000004"/>
 *             &lt;enumeration value="220000005"/>
 *             &lt;enumeration value="220000013"/>
 *             &lt;enumeration value="220000019"/>
 *             &lt;enumeration value="220000180"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="REL_ATTR" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="EXT_HW_3"/>
 *             &lt;enumeration value="EXT_HW_5"/>
 *             &lt;enumeration value="HW_SMART_VOR"/>
 *             &lt;enumeration value="RIT_NET_10"/>
 *             &lt;enumeration value="RIT_SW_16"/>
 *             &lt;enumeration value="RIT_SW_38"/>
 *             &lt;enumeration value="RIT_SW_44"/>
 *             &lt;enumeration value="SNT_SW_CLI"/>
 *             &lt;enumeration value="USS_SW_EFUAF"/>
 *             &lt;enumeration value="USS_SW_HO_62"/>
 *             &lt;enumeration value="USS_SW_IN_8"/>
 *             &lt;enumeration value="USS_SW_IT_49"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="COMMON_NAME" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="Hardware.Client.VorOrt"/>
 *             &lt;enumeration value="Hardware.Drucker.VorOrt"/>
 *             &lt;enumeration value="Hardware.Smartphone.VorOrt"/>
 *             &lt;enumeration value="Netzwerk.Server.FileSrv"/>
 *             &lt;enumeration value="Software.CB.UKVLEI"/>
 *             &lt;enumeration value="Software.Client.VorOrt"/>
 *             &lt;enumeration value="Software.HOST.Batch"/>
 *             &lt;enumeration value="Software.IA1.UKMS.Auto-Frei"/>
 *             &lt;enumeration value="Software.Notes.Technik"/>
 *             &lt;enumeration value="Software.PA1.HOST.RAIVIS.Sach-Versicherung"/>
 *             &lt;enumeration value="Software.PA2.INAS_FL"/>
 *             &lt;enumeration value="Software.Telefon.Nebenstelle.VorOrt"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "link"
})
@XmlRootElement(name = "category")
public class Category {

    @XmlElement(required = true)
    protected Link link;
    @XmlAttribute(name = "id", required = true)
    protected int id;
    @XmlAttribute(name = "REL_ATTR", required = true)
    protected String relattr;
    @XmlAttribute(name = "COMMON_NAME", required = true)
    protected String commonname;

    /**
     * Ruft den Wert der link-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Link }
     *     
     */
    public Link getLink() {
        return link;
    }

    /**
     * Legt den Wert der link-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Link }
     *     
     */
    public void setLink(Link value) {
        this.link = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der relattr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRELATTR() {
        return relattr;
    }

    /**
     * Legt den Wert der relattr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRELATTR(String value) {
        this.relattr = value;
    }

    /**
     * Ruft den Wert der commonname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMMONNAME() {
        return commonname;
    }

    /**
     * Legt den Wert der commonname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMMONNAME(String value) {
        this.commonname = value;
    }

}
