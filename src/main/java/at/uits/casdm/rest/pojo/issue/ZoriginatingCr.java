
package at.uits.casdm.rest.pojo.issue;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}link"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;enumeration value="220475405"/>
 *             &lt;enumeration value="220475411"/>
 *             &lt;enumeration value="220475419"/>
 *             &lt;enumeration value="220475420"/>
 *             &lt;enumeration value="220475423"/>
 *             &lt;enumeration value="220475431"/>
 *             &lt;enumeration value="220475432"/>
 *             &lt;enumeration value="220475438"/>
 *             &lt;enumeration value="220475439"/>
 *             &lt;enumeration value="220475444"/>
 *             &lt;enumeration value="220475447"/>
 *             &lt;enumeration value="220475449"/>
 *             &lt;enumeration value="220475450"/>
 *             &lt;enumeration value="220475452"/>
 *             &lt;enumeration value="220475454"/>
 *             &lt;enumeration value="220475468"/>
 *             &lt;enumeration value="220475470"/>
 *             &lt;enumeration value="220475474"/>
 *             &lt;enumeration value="220475476"/>
 *             &lt;enumeration value="220475477"/>
 *             &lt;enumeration value="220475485"/>
 *             &lt;enumeration value="220475488"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="REL_ATTR" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="cr:220475405"/>
 *             &lt;enumeration value="cr:220475411"/>
 *             &lt;enumeration value="cr:220475419"/>
 *             &lt;enumeration value="cr:220475420"/>
 *             &lt;enumeration value="cr:220475423"/>
 *             &lt;enumeration value="cr:220475431"/>
 *             &lt;enumeration value="cr:220475432"/>
 *             &lt;enumeration value="cr:220475438"/>
 *             &lt;enumeration value="cr:220475439"/>
 *             &lt;enumeration value="cr:220475444"/>
 *             &lt;enumeration value="cr:220475447"/>
 *             &lt;enumeration value="cr:220475449"/>
 *             &lt;enumeration value="cr:220475450"/>
 *             &lt;enumeration value="cr:220475452"/>
 *             &lt;enumeration value="cr:220475454"/>
 *             &lt;enumeration value="cr:220475468"/>
 *             &lt;enumeration value="cr:220475470"/>
 *             &lt;enumeration value="cr:220475474"/>
 *             &lt;enumeration value="cr:220475476"/>
 *             &lt;enumeration value="cr:220475477"/>
 *             &lt;enumeration value="cr:220475485"/>
 *             &lt;enumeration value="cr:220475488"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="COMMON_NAME" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;enumeration value="1182664"/>
 *             &lt;enumeration value="1182674"/>
 *             &lt;enumeration value="1182682"/>
 *             &lt;enumeration value="1182683"/>
 *             &lt;enumeration value="1182686"/>
 *             &lt;enumeration value="1182694"/>
 *             &lt;enumeration value="1182695"/>
 *             &lt;enumeration value="1182701"/>
 *             &lt;enumeration value="1182702"/>
 *             &lt;enumeration value="1182707"/>
 *             &lt;enumeration value="1182710"/>
 *             &lt;enumeration value="1182712"/>
 *             &lt;enumeration value="1182713"/>
 *             &lt;enumeration value="1182715"/>
 *             &lt;enumeration value="1182717"/>
 *             &lt;enumeration value="1182731"/>
 *             &lt;enumeration value="1182733"/>
 *             &lt;enumeration value="1182737"/>
 *             &lt;enumeration value="1182739"/>
 *             &lt;enumeration value="1182740"/>
 *             &lt;enumeration value="1182748"/>
 *             &lt;enumeration value="1182751"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "link"
})
@XmlRootElement(name = "zoriginating_cr")
public class ZoriginatingCr {

    @XmlElement(required = true)
    protected Link link;
    @XmlAttribute(name = "id", required = true)
    protected int id;
    @XmlAttribute(name = "REL_ATTR", required = true)
    protected String relattr;
    @XmlAttribute(name = "COMMON_NAME", required = true)
    protected int commonname;

    /**
     * Ruft den Wert der link-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Link }
     *     
     */
    public Link getLink() {
        return link;
    }

    /**
     * Legt den Wert der link-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Link }
     *     
     */
    public void setLink(Link value) {
        this.link = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der relattr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRELATTR() {
        return relattr;
    }

    /**
     * Legt den Wert der relattr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRELATTR(String value) {
        this.relattr = value;
    }

    /**
     * Ruft den Wert der commonname-Eigenschaft ab.
     * 
     */
    public int getCOMMONNAME() {
        return commonname;
    }

    /**
     * Legt den Wert der commonname-Eigenschaft fest.
     * 
     */
    public void setCOMMONNAME(int value) {
        this.commonname = value;
    }

}
