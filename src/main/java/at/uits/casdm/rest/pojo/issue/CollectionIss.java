
package at.uits.casdm.rest.pojo.issue;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}link" maxOccurs="unbounded"/>
 *         &lt;element ref="{}iss" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="TOTAL_COUNT" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *             &lt;enumeration value="111"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="START" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *             &lt;enumeration value="1"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="COUNT" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *             &lt;enumeration value="25"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "link",
    "iss"
})
@XmlRootElement(name = "collection_iss")
public class CollectionIss {

    @XmlElement(required = true)
    protected List<Link> link;
    @XmlElement(required = true)
    protected List<Iss> iss;
    @XmlAttribute(name = "TOTAL_COUNT", required = true)
    protected byte totalcount;
    @XmlAttribute(name = "START", required = true)
    protected byte start;
    @XmlAttribute(name = "COUNT", required = true)
    protected byte count;

    /**
     * Gets the value of the link property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the link property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLink().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Link }
     * 
     * 
     */
    public List<Link> getLink() {
        if (link == null) {
            link = new ArrayList<Link>();
        }
        return this.link;
    }

    /**
     * Gets the value of the iss property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the iss property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIss().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Iss }
     * 
     * 
     */
    public List<Iss> getIss() {
        if (iss == null) {
            iss = new ArrayList<Iss>();
        }
        return this.iss;
    }

    /**
     * Ruft den Wert der totalcount-Eigenschaft ab.
     * 
     */
    public byte getTOTALCOUNT() {
        return totalcount;
    }

    /**
     * Legt den Wert der totalcount-Eigenschaft fest.
     * 
     */
    public void setTOTALCOUNT(byte value) {
        this.totalcount = value;
    }

    /**
     * Ruft den Wert der start-Eigenschaft ab.
     * 
     */
    public byte getSTART() {
        return start;
    }

    /**
     * Legt den Wert der start-Eigenschaft fest.
     * 
     */
    public void setSTART(byte value) {
        this.start = value;
    }

    /**
     * Ruft den Wert der count-Eigenschaft ab.
     * 
     */
    public byte getCOUNT() {
        return count;
    }

    /**
     * Legt den Wert der count-Eigenschaft fest.
     * 
     */
    public void setCOUNT(byte value) {
        this.count = value;
    }

}
