
package at.uits.casdm.rest.pojo.category;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}link"/>
 *         &lt;element ref="{}isscat" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="TOTAL_COUNT" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
 *             &lt;enumeration value="642"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="START" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *             &lt;enumeration value="1"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="COUNT" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *             &lt;enumeration value="25"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "link",
    "isscat"
})
@XmlRootElement(name = "collection_isscat")
public class CollectionIsscat {

    @XmlElement(required = true)
    protected Link link;
    @XmlElement(required = true)
    protected List<Isscat> isscat;
    @XmlAttribute(name = "TOTAL_COUNT", required = true)
    protected short totalcount;
    @XmlAttribute(name = "START", required = true)
    protected byte start;
    @XmlAttribute(name = "COUNT", required = true)
    protected byte count;

    /**
     * Ruft den Wert der link-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Link }
     *     
     */
    public Link getLink() {
        return link;
    }

    /**
     * Legt den Wert der link-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Link }
     *     
     */
    public void setLink(Link value) {
        this.link = value;
    }

    /**
     * Gets the value of the isscat property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the isscat property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIsscat().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Isscat }
     * 
     * 
     */
    public List<Isscat> getIsscat() {
        if (isscat == null) {
            isscat = new ArrayList<Isscat>();
        }
        return this.isscat;
    }

    /**
     * Ruft den Wert der totalcount-Eigenschaft ab.
     * 
     */
    public short getTOTALCOUNT() {
        return totalcount;
    }

    /**
     * Legt den Wert der totalcount-Eigenschaft fest.
     * 
     */
    public void setTOTALCOUNT(short value) {
        this.totalcount = value;
    }

    /**
     * Ruft den Wert der start-Eigenschaft ab.
     * 
     */
    public byte getSTART() {
        return start;
    }

    /**
     * Legt den Wert der start-Eigenschaft fest.
     * 
     */
    public void setSTART(byte value) {
        this.start = value;
    }

    /**
     * Ruft den Wert der count-Eigenschaft ab.
     * 
     */
    public byte getCOUNT() {
        return count;
    }

    /**
     * Legt den Wert der count-Eigenschaft fest.
     * 
     */
    public void setCOUNT(byte value) {
        this.count = value;
    }

}
