
package at.uits.casdm.rest.pojo.issue;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.uits.casdm.rest.pojo.issue package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Summary_QNAME = new QName("", "summary");
    private final static QName _ResolveDate_QNAME = new QName("", "resolve_date");
    private final static QName _OpenDate_QNAME = new QName("", "open_date");
    private final static QName _RefNum_QNAME = new QName("", "ref_num");
    private final static QName _CloseDate_QNAME = new QName("", "close_date");
    private final static QName _PersistentId_QNAME = new QName("", "persistent_id");
    private final static QName _Description_QNAME = new QName("", "description");
    private final static QName _StartDate_QNAME = new QName("", "start_date");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.uits.casdm.rest.pojo.issue
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CollectionIss }
     * 
     */
    public CollectionIss createCollectionIss() {
        return new CollectionIss();
    }

    /**
     * Create an instance of {@link Link }
     * 
     */
    public Link createLink() {
        return new Link();
    }

    /**
     * Create an instance of {@link Iss }
     * 
     */
    public Iss createIss() {
        return new Iss();
    }

    /**
     * Create an instance of {@link Category }
     * 
     */
    public Category createCategory() {
        return new Category();
    }

    /**
     * Create an instance of {@link Group }
     * 
     */
    public Group createGroup() {
        return new Group();
    }

    /**
     * Create an instance of {@link LogAgent }
     * 
     */
    public LogAgent createLogAgent() {
        return new LogAgent();
    }

    /**
     * Create an instance of {@link Priority }
     * 
     */
    public Priority createPriority() {
        return new Priority();
    }

    /**
     * Create an instance of {@link Requestor }
     * 
     */
    public Requestor createRequestor() {
        return new Requestor();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link ZoriginatingCr }
     * 
     */
    public ZoriginatingCr createZoriginatingCr() {
        return new ZoriginatingCr();
    }

    /**
     * Create an instance of {@link Zowner }
     * 
     */
    public Zowner createZowner() {
        return new Zowner();
    }

    /**
     * Create an instance of {@link Assignee }
     * 
     */
    public Assignee createAssignee() {
        return new Assignee();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "summary")
    public JAXBElement<String> createSummary(String value) {
        return new JAXBElement<String>(_Summary_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "resolve_date")
    public JAXBElement<Integer> createResolveDate(Integer value) {
        return new JAXBElement<Integer>(_ResolveDate_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "open_date")
    public JAXBElement<Integer> createOpenDate(Integer value) {
        return new JAXBElement<Integer>(_OpenDate_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ref_num")
    public JAXBElement<String> createRefNum(String value) {
        return new JAXBElement<String>(_RefNum_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "close_date")
    public JAXBElement<Integer> createCloseDate(Integer value) {
        return new JAXBElement<Integer>(_CloseDate_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "persistent_id")
    public JAXBElement<String> createPersistentId(String value) {
        return new JAXBElement<String>(_PersistentId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "description")
    public JAXBElement<String> createDescription(String value) {
        return new JAXBElement<String>(_Description_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "start_date")
    public JAXBElement<Integer> createStartDate(Integer value) {
        return new JAXBElement<Integer>(_StartDate_QNAME, Integer.class, null, value);
    }

}
