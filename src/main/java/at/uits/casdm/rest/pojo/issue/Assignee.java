
package at.uits.casdm.rest.pojo.issue;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}link"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="U'0098399A8E51854EAD29859FDC9D18BD'"/>
 *             &lt;enumeration value="U'1477AA76408ADF4F9E3F767B54CA97DE'"/>
 *             &lt;enumeration value="U'30F8C363FEE96249A8D8DA7DFDC02B3F'"/>
 *             &lt;enumeration value="U'55791111189C2245AC60609F7E703FE0'"/>
 *             &lt;enumeration value="U'9AF7F359C6615D4C96036F7CCA2FB33D'"/>
 *             &lt;enumeration value="U'FA1B72CBC9CFA44382EFEA2BB5B8C1D1'"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="REL_ATTR" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="U'0098399A8E51854EAD29859FDC9D18BD'"/>
 *             &lt;enumeration value="U'1477AA76408ADF4F9E3F767B54CA97DE'"/>
 *             &lt;enumeration value="U'30F8C363FEE96249A8D8DA7DFDC02B3F'"/>
 *             &lt;enumeration value="U'55791111189C2245AC60609F7E703FE0'"/>
 *             &lt;enumeration value="U'9AF7F359C6615D4C96036F7CCA2FB33D'"/>
 *             &lt;enumeration value="U'FA1B72CBC9CFA44382EFEA2BB5B8C1D1'"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="COMMON_NAME" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="BERGER-HEDA, Rene  "/>
 *             &lt;enumeration value="GROSS, Michael  "/>
 *             &lt;enumeration value="HAMDI, Emir  "/>
 *             &lt;enumeration value="HEHLE, Oliver  "/>
 *             &lt;enumeration value="JACKEL, Michael  "/>
 *             &lt;enumeration value="MICHALICKA, Peter Ing."/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "link"
})
@XmlRootElement(name = "assignee")
public class Assignee {

    @XmlElement(required = true)
    protected Link link;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "REL_ATTR", required = true)
    protected String relattr;
    @XmlAttribute(name = "COMMON_NAME", required = true)
    protected String commonname;

    /**
     * Ruft den Wert der link-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Link }
     *     
     */
    public Link getLink() {
        return link;
    }

    /**
     * Legt den Wert der link-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Link }
     *     
     */
    public void setLink(Link value) {
        this.link = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der relattr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRELATTR() {
        return relattr;
    }

    /**
     * Legt den Wert der relattr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRELATTR(String value) {
        this.relattr = value;
    }

    /**
     * Ruft den Wert der commonname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMMONNAME() {
        return commonname;
    }

    /**
     * Legt den Wert der commonname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMMONNAME(String value) {
        this.commonname = value;
    }

}
