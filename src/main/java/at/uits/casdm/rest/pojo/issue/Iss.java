
package at.uits.casdm.rest.pojo.issue;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}link"/>
 *         &lt;choice>
 *           &lt;sequence>
 *             &lt;element ref="{}category"/>
 *             &lt;element ref="{}close_date"/>
 *             &lt;element ref="{}description"/>
 *             &lt;element ref="{}group"/>
 *             &lt;element ref="{}log_agent"/>
 *             &lt;element ref="{}open_date"/>
 *             &lt;element ref="{}persistent_id"/>
 *             &lt;element ref="{}priority"/>
 *             &lt;element ref="{}ref_num"/>
 *             &lt;element ref="{}requestor"/>
 *             &lt;element ref="{}resolve_date" minOccurs="0"/>
 *             &lt;element ref="{}start_date"/>
 *             &lt;element ref="{}status"/>
 *             &lt;element ref="{}summary"/>
 *             &lt;element ref="{}zoriginating_cr"/>
 *             &lt;element ref="{}zowner"/>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element ref="{}assignee"/>
 *             &lt;element ref="{}category"/>
 *             &lt;element ref="{}close_date"/>
 *             &lt;element ref="{}description"/>
 *             &lt;element ref="{}group"/>
 *             &lt;element ref="{}log_agent"/>
 *             &lt;element ref="{}open_date"/>
 *             &lt;element ref="{}persistent_id"/>
 *             &lt;element ref="{}priority"/>
 *             &lt;element ref="{}ref_num"/>
 *             &lt;element ref="{}requestor"/>
 *             &lt;choice>
 *               &lt;sequence>
 *                 &lt;element ref="{}start_date"/>
 *                 &lt;element ref="{}status"/>
 *                 &lt;element ref="{}summary"/>
 *                 &lt;element ref="{}zoriginating_cr"/>
 *                 &lt;element ref="{}zowner"/>
 *               &lt;/sequence>
 *               &lt;sequence>
 *                 &lt;element ref="{}resolve_date"/>
 *                 &lt;element ref="{}start_date"/>
 *                 &lt;element ref="{}status"/>
 *                 &lt;element ref="{}summary"/>
 *                 &lt;element ref="{}zoriginating_cr" minOccurs="0"/>
 *                 &lt;element ref="{}zowner"/>
 *               &lt;/sequence>
 *             &lt;/choice>
 *           &lt;/sequence>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *             &lt;enumeration value="220244769"/>
 *             &lt;enumeration value="220244770"/>
 *             &lt;enumeration value="220244771"/>
 *             &lt;enumeration value="220244772"/>
 *             &lt;enumeration value="220244773"/>
 *             &lt;enumeration value="220244774"/>
 *             &lt;enumeration value="220244775"/>
 *             &lt;enumeration value="220244776"/>
 *             &lt;enumeration value="220244777"/>
 *             &lt;enumeration value="220244778"/>
 *             &lt;enumeration value="220244779"/>
 *             &lt;enumeration value="220244780"/>
 *             &lt;enumeration value="220244781"/>
 *             &lt;enumeration value="220244782"/>
 *             &lt;enumeration value="220244783"/>
 *             &lt;enumeration value="220244784"/>
 *             &lt;enumeration value="220244785"/>
 *             &lt;enumeration value="220244786"/>
 *             &lt;enumeration value="220244787"/>
 *             &lt;enumeration value="220244788"/>
 *             &lt;enumeration value="220244789"/>
 *             &lt;enumeration value="220244790"/>
 *             &lt;enumeration value="220244791"/>
 *             &lt;enumeration value="220244792"/>
 *             &lt;enumeration value="220244793"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="REL_ATTR" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="iss:220244769"/>
 *             &lt;enumeration value="iss:220244770"/>
 *             &lt;enumeration value="iss:220244771"/>
 *             &lt;enumeration value="iss:220244772"/>
 *             &lt;enumeration value="iss:220244773"/>
 *             &lt;enumeration value="iss:220244774"/>
 *             &lt;enumeration value="iss:220244775"/>
 *             &lt;enumeration value="iss:220244776"/>
 *             &lt;enumeration value="iss:220244777"/>
 *             &lt;enumeration value="iss:220244778"/>
 *             &lt;enumeration value="iss:220244779"/>
 *             &lt;enumeration value="iss:220244780"/>
 *             &lt;enumeration value="iss:220244781"/>
 *             &lt;enumeration value="iss:220244782"/>
 *             &lt;enumeration value="iss:220244783"/>
 *             &lt;enumeration value="iss:220244784"/>
 *             &lt;enumeration value="iss:220244785"/>
 *             &lt;enumeration value="iss:220244786"/>
 *             &lt;enumeration value="iss:220244787"/>
 *             &lt;enumeration value="iss:220244788"/>
 *             &lt;enumeration value="iss:220244789"/>
 *             &lt;enumeration value="iss:220244790"/>
 *             &lt;enumeration value="iss:220244791"/>
 *             &lt;enumeration value="iss:220244792"/>
 *             &lt;enumeration value="iss:220244793"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="COMMON_NAME" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="262671-i"/>
 *             &lt;enumeration value="262672-i"/>
 *             &lt;enumeration value="262673-i"/>
 *             &lt;enumeration value="262674-i"/>
 *             &lt;enumeration value="262675-i"/>
 *             &lt;enumeration value="262676-i"/>
 *             &lt;enumeration value="262677-i"/>
 *             &lt;enumeration value="262678-i"/>
 *             &lt;enumeration value="262679-i"/>
 *             &lt;enumeration value="262680-i"/>
 *             &lt;enumeration value="262681-i"/>
 *             &lt;enumeration value="262682-i"/>
 *             &lt;enumeration value="262683-i"/>
 *             &lt;enumeration value="262684-i"/>
 *             &lt;enumeration value="262685-i"/>
 *             &lt;enumeration value="262686-i"/>
 *             &lt;enumeration value="262687-i"/>
 *             &lt;enumeration value="262688-i"/>
 *             &lt;enumeration value="262689-i"/>
 *             &lt;enumeration value="262690-i"/>
 *             &lt;enumeration value="262691-i"/>
 *             &lt;enumeration value="262692-i"/>
 *             &lt;enumeration value="262693-i"/>
 *             &lt;enumeration value="262694-i"/>
 *             &lt;enumeration value="262695-i"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content"
})
@XmlRootElement(name = "iss")
public class Iss {

    @XmlElementRefs({
        @XmlElementRef(name = "open_date", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "start_date", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "zoriginating_cr", type = ZoriginatingCr.class, required = false),
        @XmlElementRef(name = "priority", type = Priority.class, required = false),
        @XmlElementRef(name = "summary", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "zowner", type = Zowner.class, required = false),
        @XmlElementRef(name = "category", type = Category.class, required = false),
        @XmlElementRef(name = "close_date", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "description", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "assignee", type = Assignee.class, required = false),
        @XmlElementRef(name = "persistent_id", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "group", type = Group.class, required = false),
        @XmlElementRef(name = "log_agent", type = LogAgent.class, required = false),
        @XmlElementRef(name = "resolve_date", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "link", type = Link.class, required = false),
        @XmlElementRef(name = "requestor", type = Requestor.class, required = false),
        @XmlElementRef(name = "ref_num", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "status", type = Status.class, required = false)
    })
    protected List<Object> content;
    @XmlAttribute(name = "id", required = true)
    protected int id;
    @XmlAttribute(name = "REL_ATTR", required = true)
    protected String relattr;
    @XmlAttribute(name = "COMMON_NAME", required = true)
    protected String commonname;

    /**
     * Ruft das restliche Contentmodell ab. 
     * 
     * <p>
     * Sie rufen diese "catch-all"-Eigenschaft aus folgendem Grund ab: 
     * Der Feldname "Category" wird von zwei verschiedenen Teilen eines Schemas verwendet. Siehe: 
     * Zeile 530 von file:/C:/Data/WorkspaceTryouts/src/main/resources/Issue.xsd
     * Zeile 511 von file:/C:/Data/WorkspaceTryouts/src/main/resources/Issue.xsd
     * <p>
     * Um diese Eigenschaft zu entfernen, wenden Sie eine Eigenschaftenanpassung f�r eine
     * der beiden folgenden Deklarationen an, um deren Namen zu �ndern: 
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * {@link ZoriginatingCr }
     * {@link Priority }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Zowner }
     * {@link Category }
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Assignee }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Group }
     * {@link LogAgent }
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * {@link Link }
     * {@link Requestor }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Status }
     * 
     * 
     */
    public List<Object> getContent() {
        if (content == null) {
            content = new ArrayList<Object>();
        }
        return this.content;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der relattr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRELATTR() {
        return relattr;
    }

    /**
     * Legt den Wert der relattr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRELATTR(String value) {
        this.relattr = value;
    }

    /**
     * Ruft den Wert der commonname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMMONNAME() {
        return commonname;
    }

    /**
     * Legt den Wert der commonname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMMONNAME(String value) {
        this.commonname = value;
    }

}
