/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.service;

import at.uits.casdm.axis.USD_WebServiceSoap_PortType;

/**
 * Created by VZM9621 on 03.06.2016.
 */
public class CasdmServiceAccess {

    public int sid;
    public USD_WebServiceSoap_PortType service;

    public CasdmServiceAccess(int sid, USD_WebServiceSoap_PortType service) {
        this.sid = sid;
        this.service = service;
    }
}
