/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.service;

import at.uits.casdm.pojo.rest.RestAccess;
import at.uits.casdm.pojo.rest.RestObjectFactory;
import at.uits.casdm.rest.pojo.category.CategoryObjectFactory;
import at.uits.casdm.rest.pojo.category.CollectionIsscat;
import at.uits.casdm.rest.pojo.category.Isscat;
import at.uits.casdm.rest.pojo.issue.CollectionIss;
import at.uits.casdm.rest.pojo.issue.ObjectFactory;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Created by VZM9621 on 30.05.2016.
 */
public class CasdmRestConnector {

    private final String baseuri;  //     = "http://tw2k12usd2012s1.integration.uniqa.at:8090/caisd-rest";
    private final String username; //    = "usd_web_service";
    private final String password; //    = "tng@W2008";
    private final int TIMEOUT = 60000;

    private static final Logger LOG = LoggerFactory.getLogger(CasdmRestConnector.class);

    public CasdmRestConnector(String baseuri, String username, String password) {
        this.baseuri = baseuri;
        this.username = username;
        this.password = password;
    }

    private HttpClient getHttpClient() {
        // create requestconfig and set timeout
        RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                .setSocketTimeout(TIMEOUT)
                .setConnectTimeout(TIMEOUT)
                .setConnectionRequestTimeout(TIMEOUT)
                .build();
        // create client
        HttpClient client = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .build();
        return client;
    }

    public int login() {
        HttpClient client = getHttpClient();

        // Encode the username/password
        String encodedCredentials = new String(Base64.encodeBase64((this.username + ":" + this.password).getBytes()));
        // Use POST method when requesting an Access Key
        HttpPost post = new HttpPost(baseuri + "/rest_access");
        post.addHeader("Accept" , "application/xml");
        post.addHeader("Content-Type", "application/xml; charset=UTF-8");
        post.addHeader("Authorization" , "Basic " + encodedCredentials);
        post.setEntity(new StringEntity("<rest_access/>", StandardCharsets.UTF_8));
        try {
            // Execute request
            HttpResponse response = client.execute(post);
            LOG.debug("Response status code: " + response.getStatusLine());
            LOG.debug("Response body: " + response.getEntity());
            String xml = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            RestAccess restAccess = CasdmUtils.unmarshallXml(xml, new RestObjectFactory().createRestAccess().getClass());
            int accessKey = restAccess.getAccessKey();
            LOG.debug("RestAccessKey:" + accessKey);
            post.releaseConnection();
            return accessKey;
        } catch (IOException e) {
            LOG.error(e.getMessage());
        } finally {
            post.releaseConnection();
        }
        return 0;
    }

    // &size=10
    public void getIssues() throws IOException {
        long from = CasdmUtils.getTimestamp(2016, 1, 3, 0, 0);
        long to = CasdmUtils.getTimestamp(2016, 1, 6, 7, 0);

        int start = 1000;
        HttpClient client = getHttpClient();

        String sql = "open_date > " + from + " AND open_date < " + to; //getINCategoryClause();
        String uri = "http://tw2k12usd2012s1.integration.uniqa.at:8090/caisd-rest/iss?size=" + start + "&WC=" + URLEncoder.encode(sql);
        System.out.println("Execute: " + uri);

        HttpGet get = new HttpGet(uri);
        get.addHeader("X-AccessKey", String.valueOf(login()));
        get.addHeader("Accept", "application/xml");
        get.addHeader("X-Obj-Attrs", "id,ref_num,persistent_id,zoriginating_cr,description,open_date,start_date,close_date,resolve_date,group,zowner,category,summary,log_agent,requestor,status,priority,assignee");
        HttpResponse response = client.execute(get);

        String xml = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
        System.out.println(xml);
        CollectionIss result = CasdmUtils.unmarshallXml(xml, new ObjectFactory().createCollectionIss().getClass());
        List<Object> content = result.getIss().get(0).getContent();
        for(Object o : content)
            System.out.println(o);
    }

    public void getValidCategoryIds() throws IOException {
        int sid = login();
        String wc = CasdmUtils.getINCategoryClauseWithPrefix("sym ");
        System.out.println("WC: " + wc);
        String uri = "http://tw2k12usd2012s1.integration.uniqa.at:8090/caisd-rest/isscat";
        System.out.println(uri);

        HttpGet get = new HttpGet(uri);
        get.addHeader("X-AccessKey", String.valueOf(sid));
        get.addHeader("Accept" , "application/xml");
        get.addHeader("X-Obj-Attrs", "sym");
        HttpResponse response = getHttpClient().execute(get);
        String xml = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
        CollectionIsscat issueCategoriesCollection = CasdmUtils.unmarshallXml(xml, new CategoryObjectFactory().createCollectionIsscat().getClass());
        List<Isscat> list = issueCategoriesCollection.getIsscat();
        for(Isscat isscat : list) {
            System.out.println(isscat.getId() + " = " + isscat.getSym());
        }
        System.out.println(xml);
    }


    public static void main(String args[]) throws IOException {
        CasdmRestConnector connector = new CasdmRestConnector(
                "http://tw2k12usd2012s1.integration.uniqa.at:8090/caisd-rest",
                "usd_web_service",
                "tng@W2008");

        connector.getIssues();
    }
}
