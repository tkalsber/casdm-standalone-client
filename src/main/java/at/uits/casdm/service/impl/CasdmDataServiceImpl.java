/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.service.impl;

import at.uits.casdm.pojo.Attachment;
import at.uits.casdm.pojo.Issue;
import at.uits.casdm.service.AttachmentProcessor;
import at.uits.casdm.service.CasdmDataService;

import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by VZM9621 on 03.06.2016.
 */
public class CasdmDataServiceImpl implements CasdmDataService {

    @Override
    public List<Issue> getIssues(long from, long to) throws ServiceException, RemoteException {
        return null;
    }

    @Override
    public void getAttachment(Attachment attachment, AttachmentProcessor attachmentProcessor) {

    }

}
