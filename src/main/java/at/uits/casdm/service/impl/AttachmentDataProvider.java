/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.service.impl;

import at.uits.casdm.pojo.Attachment;
import at.uits.casdm.pojo.rest.RestAccess;
import at.uits.casdm.pojo.rest.RestObjectFactory;
import at.uits.casdm.service.AttachmentProcessor;
import at.uits.casdm.service.CasdmUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Created by VZM9621 on 30.05.2016.
 */
public class AttachmentDataProvider {

    private final String baseuri;  //     = "http://tw2k12usd2012s1.integration.uniqa.at:8090/caisd-rest";
    private final String username; //    = "usd_web_service";
    private final String password; //    = "tng@W2008";
    private final int TIMEOUT = 60000;

    private static final Logger LOG = LoggerFactory.getLogger(AttachmentDataProvider.class);

    public AttachmentDataProvider(String baseuri, String username, String password) {
        this.baseuri = baseuri;
        this.username = username;
        this.password = password;
    }

    private RequestConfig requestConfigWithTimeout(int timeoutInMilliseconds) {
        return RequestConfig.copy(RequestConfig.DEFAULT)
                .setSocketTimeout(timeoutInMilliseconds)
                .setConnectTimeout(timeoutInMilliseconds)
                .setConnectionRequestTimeout(timeoutInMilliseconds)
                .build();
    }

    /**
     * long attachmentId = 220164561;
     * @param attachment
     * @param attachmentProcessor
     */
    public void getAttachment(Attachment attachment, AttachmentProcessor attachmentProcessor) {
        // create requestconfig and set timeout
        RequestConfig requestConfig = requestConfigWithTimeout(TIMEOUT);
        // create client
        HttpClient client = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .build();
        // Encode the username/password
        String encodedCredentials = new String(Base64.encodeBase64((username + ":" + password).getBytes()));
        // Use POST method when requesting an Access Key
        HttpPost post = new HttpPost(baseuri + "/rest_access");
        post.addHeader("Accept" , "application/xml");
        post.addHeader("Content-Type", "application/xml; charset=UTF-8");
        post.addHeader("Authorization" , "Basic " + encodedCredentials);
        post.setEntity(new StringEntity("<rest_access/>", StandardCharsets.UTF_8));
        try {
            // Execute request
            HttpResponse response = client.execute(post);
            LOG.debug("Response status code: " + response.getStatusLine());
            LOG.debug("Response body: " + response.getEntity());
            String xml = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            RestAccess restAccess = CasdmUtils.unmarshallXml(xml, new RestObjectFactory().createRestAccess().getClass());
            int accessKey = restAccess.getAccessKey();
            LOG.debug("RestAccessKey:" + accessKey);
            String uri = "http://tw2k12usd2012s1.integration.uniqa.at:8090/caisd-rest" + "/attmnt/" + attachment.getId() + "/file-resource";
            LOG.debug("Download attachment url:" + uri);
            post.releaseConnection();

            HttpGet get = new HttpGet(uri);
            get.addHeader("X-AccessKey", String.valueOf(accessKey));
            HttpResponse getResponse = client.execute(get);
            HttpEntity attachmentEntity = getResponse.getEntity();
            attachmentProcessor.processAttachment(attachment, attachmentEntity);
            // Files.copy(entity.getContent(), Paths.get("C:\\Temp\\mydoctest.pdf"));
            get.releaseConnection();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        } finally {
            post.releaseConnection();
        }
    }

    public void getAttachment(long aid) {
        // create requestconfig and set timeout
        RequestConfig requestConfig = requestConfigWithTimeout(TIMEOUT);
        // create client
        HttpClient client = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .build();
        // Encode the username/password
        String encodedCredentials = new String(Base64.encodeBase64((username + ":" + password).getBytes()));
        // Use POST method when requesting an Access Key
        HttpPost post = new HttpPost(baseuri + "/rest_access");
        post.addHeader("Accept" , "application/xml");
        post.addHeader("Content-Type", "application/xml; charset=UTF-8");
        post.addHeader("Authorization" , "Basic " + encodedCredentials);
        post.setEntity(new StringEntity("<rest_access/>", StandardCharsets.UTF_8));
        try {
            // Execute request
            HttpResponse response = client.execute(post);
            LOG.debug("Response status code: " + response.getStatusLine());
            LOG.debug("Response body: " + response.getEntity());
            String xml = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            RestAccess restAccess = CasdmUtils.unmarshallXml(xml, new RestObjectFactory().createRestAccess().getClass());
            int accessKey = restAccess.getAccessKey();
            LOG.debug("RestAccessKey:" + accessKey);
            String uri = "http://tw2k12usd2012s1.integration.uniqa.at:8090/caisd-rest" + "/attmnt/" + aid + "/file-resource";
            LOG.debug("Download attachment url:" + uri);
            post.releaseConnection();

            HttpGet get = new HttpGet(uri);
            get.addHeader("X-AccessKey", String.valueOf(accessKey));
            HttpResponse getResponse = client.execute(get);
            HttpEntity attachmentEntity = getResponse.getEntity();
            Files.copy(attachmentEntity.getContent(), Paths.get("C:\\Temp\\mydoctest.pdf"), REPLACE_EXISTING);
            get.releaseConnection();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        } finally {
            post.releaseConnection();
        }
    }

    public static void main(String args[]) {
        new AttachmentDataProvider("http://tw2k12usd2012s1.integration.uniqa.at:8090/caisd-rest", "usd_web_service", "tng@W2008").getAttachment(220164561l);
    }

}
