/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.service;

/**
 * Represents a row in the CSV file, which was created by transformation
 * of the original Excel-file called 'SD_mapping_werteliste_V2.xlsx'.
 *
 * Created by VZM9621 on 21.10.2016.
 */
class MappingRecord {

    String kategorieOderGruppe = "";
    String assignee = "";
    String catResp = "";
    String projectKey = "";
    String product = "";
    String productCostId = "";
    String activityKey = "";

    @Override
    public String toString() {
        return "\nMappingRecord("
                + "\n\tcategory or group: " + kategorieOderGruppe
                + "\n\tassignee: " + assignee
                + "\n\tcatResp: " + catResp
                + "\n\tprojectKey: " + projectKey
                + "\n\tproduct: " + product
                + "\n\tproductCostId: " + productCostId
                + "\n\tactivityKey: " + activityKey
                + ")";
    }
}
