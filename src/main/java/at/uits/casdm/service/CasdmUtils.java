/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.service;

import at.uits.casdm.axis.ArrayOfString;
import at.uits.casdm.pojo.Attribute;
import at.uits.casdm.pojo.UDSObject;
import at.uits.casdm.pojo.UDSObjectList;
import at.uits.casdm.report.ReportColumn;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.apache.http.client.config.RequestConfig;

import javax.annotation.Nonnull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.*;
import java.util.Date;
import java.util.List;

/**
 * Created by VZM9621 on 18.05.2016.
 */
public class CasdmUtils {

    /**
     * Create a <code>RequestConfig</code> instance to set the timeout for the current request.
     *
     * Socket Timeout - this is the time of inactivity to wait for packets to arrive
     * Connection Timeout - the time to establish a connection with the remote host
     * Connection Manager Timeout - the time to fetch a connection from the connection pool
     *
     * @param timeoutInMilliseconds timeout in milliseconds
     *
     * @return a requestconfig instance
     */
    public static RequestConfig requestConfigWithTimeout(int timeoutInMilliseconds) {
        return RequestConfig.copy(RequestConfig.DEFAULT)
                .setSocketTimeout(timeoutInMilliseconds)
                .setConnectTimeout(timeoutInMilliseconds)
                .setConnectionRequestTimeout(timeoutInMilliseconds)
                .build();
    }

    public static <T> T unmarshallXml(final String xml, Class<T> clazz) {
        JAXBContext context = null;
        try {
            context = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            T unmarshalledObject = (T) unmarshaller.unmarshal(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
            return unmarshalledObject;
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void printUDSObjectList(UDSObjectList udsObjectList) {
        if(udsObjectList ==  null) return;
        List<UDSObject> items = udsObjectList.getUDSObject();
        for (UDSObject item : items) {
            printUDSObject(item);
        }
    }

    public static void printUDSObject(UDSObject udsObject) {
        if(udsObject == null) return;
        List<Attribute> attributes = udsObject.getAttributes().getAttribute();
        for (Attribute attribute : attributes) {
            System.out.println(attribute);
        }
    }

    public static long getTimestamp(int year, int month, int day, int hour, int minute) {
        ZoneId vienna = ZoneId.of("Europe/Vienna");
        LocalDateTime time = LocalDateTime.of(year, month, day, hour, minute);
        ZonedDateTime viennaDateTime = ZonedDateTime.of(time, vienna);
        return getTimestamp(viennaDateTime);
    }

    public static long getTimestamp(@Nonnull ZonedDateTime zonedTime) {
        Instant instant = zonedTime.toInstant();
        Date date = Date.from(instant);
        System.out.println(date);
        return date.getTime()/1000;
    }


    public static String getTimestampAsString(int year, int month, int day, int hour, int minute) {
        return String.valueOf(getTimestamp(year, month, day, hour, minute));
    }
/*
    public static String getTimestampAsString(@Nonnull LocalDateTime time) {
        ZonedDateTime zdt = Zon
        return String.valueOf(getTimestamp(time));
    }
*/
    public static void persist(Serializable obj, File file) {
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(obj);
            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                oos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static <T> T load(File file) {
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new FileInputStream(file));
            T type = (T) ois.readObject();
            return type;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            try {
                ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static ArrayOfString getIssuetableColumns() {
        return new ArrayOfString(
                new String[] {
                        ReportColumn.ID.getInternalName(),
                        ReportColumn.ISSUE_NR.getInternalName(),
                        ReportColumn.INCIDENT_NR.getInternalName(),
                        ReportColumn.GROUP.getInternalName(),
                        ReportColumn.OWNER.getInternalName(),
                        ReportColumn.DESCRIPTION.getInternalName(),
                        ReportColumn.SUMMARY.getInternalName(),
                        ReportColumn.ENDUSER_LASTNAME.getInternalName(),
                        ReportColumn.ENDUSER_FIRSTNAME.getInternalName(),
                        ReportColumn.ENDUSER_ID.getInternalName(),
                        ReportColumn.REPORTER_FIRSTNAME.getInternalName(),
                        ReportColumn.REPORTER_LASTNAME.getInternalName(),
                        ReportColumn.REPORTER_ID.getInternalName(),
                        ReportColumn.STATUS_ID.getInternalName(),
                        ReportColumn.STATUS_NAME.getInternalName(),
                        ReportColumn.CATEGORY.getInternalName(),
                        ReportColumn.PRIORITY.getInternalName(),
                        ReportColumn.PERSISTENCE_ID.getInternalName(),
                        ReportColumn.CREATED_DATE.getInternalName(),
                        ReportColumn.STARTED_DATE.getInternalName(),
                        ReportColumn.RESOLVED_DATE.getInternalName(),
                        ReportColumn.CLOSED_DATE.getInternalName(),
                        ReportColumn.ASSIGNEE_FIRSTNAME.getInternalName(),
                        ReportColumn.ASSIGNEE_LASTNAME.getInternalName(),
                        ReportColumn.ASSIGNEE_ID.getInternalName(),
                        ReportColumn.PHONE_NUMBER.getInternalName(),
                        ReportColumn.MOBILE_NUMBER.getInternalName()
                });
    }

    public static String getINCategoryClause() {
        return getINCategoryClauseWithPrefix(" AND category.sym");
    }

    public static String getINCategoryClauseWithPrefix(final String prefix) {
        InputStream in = CasdmUtils.class.getResourceAsStream("/valid-categories.txt");
        BufferedReader bfr = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        String line = null;
        List<String> categories = Lists.newArrayList();
        try {
            while( (line = bfr.readLine()) != null)
                categories.add("'" + line +  "'");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Joiner joiner = Joiner.on(", ");

        String sql = prefix + " IN (" + joiner.join(categories) + ")";
        return sql;
    }
}
