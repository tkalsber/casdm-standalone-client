/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.service;

import at.uits.casdm.axis.ArrayOfString;
import at.uits.casdm.axis.ListResult;
import at.uits.casdm.axis.USD_WebServiceLocator;
import at.uits.casdm.axis.USD_WebServiceSoap_PortType;
import at.uits.casdm.pojo.*;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.rpc.ServiceException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by VZM9621 on 12.05.2016.
 */
public class CasdmConnector {

    private final String restEndpoint; //    = "http://tw2k12usd2012s1.integration.uniqa.at:8090/caisd-rest/rest_access";
    private final String soapEndpoint;
    private final String username; //    = "usd_web_service";
    private final String password; //    = "tng@W2008";
    private CasdmServiceAccess serviceAccess;

    private static final Logger LOG = LoggerFactory.getLogger(CasdmConnector.class);

    public CasdmConnector(String restEndpoint, String soapEndpoint, String username, String password) {
        this.restEndpoint = restEndpoint;
        this.soapEndpoint = soapEndpoint;
        this.username = username;
        this.password = password;
    }

    public CasdmServiceAccess getServiceAccess() {
        if(this.serviceAccess == null)
            initService();
        return serviceAccess;
    }

    private void initService() {
        USD_WebServiceLocator locator = new USD_WebServiceLocator();

        USD_WebServiceSoap_PortType service = null;
        try {
            if(soapEndpoint == null) service = locator.getUSD_WebServiceSoap();
            else service = locator.getUSD_WebServiceSoap(new URL(soapEndpoint));
            int sid = service.login(username, password);
            this.serviceAccess = new CasdmServiceAccess(sid, service);
        } catch (ServiceException e) {
            LOG.error(e.getMessage());
        } catch (RemoteException e) {
            LOG.error(e.getMessage());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * // 10/22/2015 04:11 pm
     * long from = CasdmUtils.getTimestamp(2015,10,22,14,10);
     * long to = CasdmUtils.getTimestamp(2015,10,22,14,12);
     *
     * @return
     */
    public List<Issue> getIssues(long from, long to) throws ServiceException, RemoteException {

        CasdmServiceAccess serviceAccess = this.getServiceAccess();
        if(serviceAccess == null) {
            System.out.println("can not access casdm");
            return Lists.newArrayList();
        }

        // load issues created between from (inclusive) and to (exclusive)
        String sql = "zowner.name = 'Owner_UNIQA' AND status.sym = 'New' AND open_date >= " + from + " AND open_date <= " + to + getINCategoryClause();
        if(true) System.out.println("SQL=\n\n" + sql);


        ListResult result = serviceAccess.service.doQuery(serviceAccess.sid, "iss", sql);
        int handle = result.getListHandle();
        String xml = serviceAccess.service.getListValues(serviceAccess.sid, handle, 0, -1, CasdmUtils.getIssuetableColumns());
        System.out.println("XML=" + xml);
        UDSObjectList objList = CasdmUtils.unmarshallXml(xml, new ObjectFactory().createUDSObjectList().getClass());
        List<Issue> issues = null;
        if(objList != null) {
            //CasdmUtils.printUDSObjectList(objList);
            issues = IssueFactory.getIssuesFromUDSObjectList(objList);
            System.out.println("Issues loaded:" + issues.size());
        }
        else System.out.println("Objlist is null !!!");

        // add properties to issues
        this.addIssueProperties(issues);

        // add attachments to issues
        //this.addIssueAttachments(issues);

        // return final issues with properties and attachments
        return issues;
    }

    private void addIssueProperties(List<Issue> issues) throws ServiceException, RemoteException {
        // get service access
        CasdmServiceAccess serviceAccess = this.getServiceAccess();

        // load properties for each issue
        for(Issue issue : issues) {
            String sql = "owning_iss = '" + issue.getPersistenceId() +"'";
            ListResult result = serviceAccess.service.doQuery(serviceAccess.sid, "iss_prp", sql);
            int handle = result.getListHandle();

            String xml = serviceAccess.service.getListValues(serviceAccess.sid, handle, 0, -1, new ArrayOfString(new String[] { "label", "value", "description" }));
            UDSObjectList objList = CasdmUtils.unmarshallXml(xml, new ObjectFactory().createUDSObjectList().getClass());
            List<IssueProperty> issueProperties = Lists.newArrayList();
            for(UDSObject obj : objList.getUDSObject()) {
                issueProperties.add(new IssueProperty(
                        obj.getAttributeByName("label").getAttrValue(),
                        obj.getAttributeByName("value").getAttrValue(),
                        obj.getAttributeByName("description").getAttrValue()));
            }
            issue.getProperties().addAll(issueProperties);
        }
    }

    private void addIssueAttachments(List<Issue> issues) throws ServiceException, RemoteException {

        // get service access
        CasdmServiceAccess serviceAccess = this.getServiceAccess();

        for(Issue issue : issues) {

            // get issue_attachments relation
            String sql = "iss = '" + issue.getPersistenceId() +"'";
            ListResult result = serviceAccess.service.doQuery(serviceAccess.sid, "lrel_attachments_issues", sql);
            String xml = serviceAccess.service.getListValues(serviceAccess.sid, result.getListHandle(), 0, -1, new ArrayOfString());
            UDSObjectList objList = CasdmUtils.unmarshallXml(xml, new ObjectFactory().createUDSObjectList().getClass());

            // extract related attachment ids and get attachments
            List<String> attmntIds = objList.getAttributeValuesByName("attmnt");
            if(attmntIds.size() == 0) {
                System.out.println("Issue has no attachments:" + issue.getIssueNr());
                continue;
            }
            Joiner joiner = Joiner.on(", ");
            sql = "id IN (" + joiner.join(attmntIds) + ")";
            System.out.println("SQL ATTACHMENTS:" + sql);
            result = serviceAccess.service.doQuery(serviceAccess.sid, "attmnt", sql);

            final ArrayOfString ATTR_COLNAMES = new ArrayOfString(new String[] {
                    "id",
                    "attmnt_name",
                    "repository",
                    "file_type",
                    "file_size",
                    "file_name",
                    "orig_file_name",
                    "persistent_id",
                    "repository"
            });

            xml = serviceAccess.service.getListValues(serviceAccess.sid, result.getListHandle(), 0, -1, ATTR_COLNAMES);
            objList = CasdmUtils.unmarshallXml(xml, new ObjectFactory().createUDSObjectList().getClass());
            List<Attachment> attachments = IssueFactory.getAttachmentsFromUDSObjectList(objList);
            issue.getAttachments().addAll(attachments);

            /*
            System.out.println("\nDOCUMENT REPOSITORIES\n");
            for(Attachment attachment : attachments) {
                sql = "persistent_id = '" + attachment.getRepository() +"'";
                result = service.doQuery(sid, "doc_rep", sql);
                xml = service.getListValues(sid, result.getListHandle(), 0, -1, new ArrayOfString());
                objList = CasdmUtils.unmarshallXml(xml, new ObjectFactory().createUDSObjectList().getClass());
                CasdmUtils.printUDSObjectList(objList);
            }
            */
            /*
             http://hostname:8080/CAisd/UploadServlet?AttmntId=" + attmntId
                + "&Bpsid=" + bopsid
                + "&retURL=http://hostname/CAisd/pdmweb.exe?SID=" + sid
                + "+FID=" + new Random().nextInt()
            */
        }
    }

    private static String getINCategoryClause() {
        // AND
        String category =  getINCategoryClauseWithPrefix(" category.sym");
        String group = getINCategoryClauseWithPrefix( " group.last_name");

        return " AND ( " + category + " OR " + group + " ) ";
    }

    private static String getINCategoryClauseWithPrefix(final String prefix) {
        InputStream in = CasdmConnector.class.getResourceAsStream("/valid-categories.txt");
        BufferedReader bfr = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        String line = null;
        List<String> categories = Lists.newArrayList();
        try {
            while( (line = bfr.readLine()) != null)
                categories.add("'" + line +  "'");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Joiner joiner = Joiner.on(", ");
        // group.last_name

        String sql = prefix + " IN (" + joiner.join(categories) + ")";
        return sql;
    }

    public static void main(String args[]) throws ServiceException, IOException {

        //checkDownloaded();

        long from = CasdmUtils.getTimestamp(2016,10, 1,  23,00);
        long to = CasdmUtils.getTimestamp(  2016,10, 10, 00,00);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        LocalDateTime ldt2 = LocalDateTime.parse("2016-09-07 12:36", formatter);
        Date fromDate = Date.from(ldt2.atZone(ZoneId.of("Europe/Vienna")).toInstant());

        //Instant instant = Instant.ofEpochMilli(date.getTime());
        //LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
        //Instant instant = Instant.ofEpochMilli(date.getTime());
        //LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);

        final String DEV_REST_URL  = "http://tw2k12usd2012s1.integration.uniqa.at:8090/caisd-rest/rest_access";
        final String TEST_REST_URL = "http://tw2k12usd2012s2.integration.uniqa.at:8090/caisd-rest/rest_access";
        final String PROD_REST_URL = "http://WS00527.PROD01.R-DC.NET:8090/caisd-rest/rest_access";
        final String CASDM_REST_URL = "http://servicedesk.uniqa.at:8090/caisd-rest/rest_access";

        final String[] DEV_CREDENTIALS  = { "usd_web_service", "tng@W2008" };
        final String[] TEST_CREDENTIALS = { "jira_web_service", "JNkiEeNN.r@s" };
        final String[] PROD_CREDENTIALS = { "jira_web_service", "JNkiEeNN.r@s" };

        final String DEV_SOAP_URL  = "http://tw2k12usd2012s1.integration.uniqa.at:8080/axis/services/USD_R11_WebService";
        final String TEST_SOAP_URL = "http://tw2k12usd2012s2.integration.uniqa.at:8080/axis/services/USD_R11_WebService";
        final String PROD_SOAP_URL = "http://WS00527.PROD01.R-DC.NET:8080/axis/services/USD_R11_WebService";
        final String CASDM_SOAP_URL = "http://servicedesk.uniqa.at:8080/axis/services/USD_R11_WebService";

        CasdmConnector connector = new CasdmConnector(
                TEST_REST_URL,
                TEST_SOAP_URL,
                TEST_CREDENTIALS[0],
                TEST_CREDENTIALS[1]);

        List<Issue> issues = connector.getIssues(from, to);
        System.out.println("#Issues:" + issues.size());

        for(Issue issue : issues)
           System.out.println("[ \"" + issue.getIncidentNr() + "/" + issue.getIssueNr() + "\""
                   + ", \"" + issue.getPriority() + "\""
                   + ", \"" + issue.getGroup() + "\""
                   + ", \"" + issue.getCategory() +  "\" ]");
    }

    private static void checkDownloaded() throws IOException {
        String[] categories = new String[]{

                "Software.PA1.COBRA-KFZ",
                "Software.CS2.BPMS.EBF",
                "Software.IA1.UKMS",
                "Software.CS2.DMS-AT",
                "Software.IA1.ATLAS",
                "Software.PA1.COBRA-KFZ",
                "Software.IA1.UKMS.Auto-Frei",
                "Software.IA1.UKMS",
                "Software.IA1.Sicherheitsoption.Allgemeine_Haftpflicht",
                "Software.CB.SLS-BreitenHaft",
                "Software.IA1.ATLAS",
                "Software.IA1.ATLAS",
                "Software.IA1.Sicherheitsoption.Allgemeine_Haftpflicht",
                "Software.IA2.CallCenter",
                "Software.CB.SLS-BreitenHaft"

        };

        BufferedReader bfr = new BufferedReader(new InputStreamReader(CasdmConnector.class.getResourceAsStream("/newcategories.txt")));
        ArrayList<String> cat = Lists.newArrayList();
        String line = "";
        while( (line = bfr.readLine()) != null)
            cat.add(line);
        System.out.println("Lines loaded:" + cat.size());

        for(String category : categories)
            System.out.println(category + " : " + cat.contains(category));
    }
}
