/*
 * Copyright (c) 2016 Thomas Kalsberger, M.Sc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.uits.casdm.service;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * This is an extension of the MappingRecord, which has a additional
 * key-field, used to identify the record. Categories are unique and therefore
 * used as key.
 *
 * Created by VZM9621 on 21.10.2016.
 */
class SortableMappingRecord {

    final String key;
    final MappingRecord mappingValues;

    public SortableMappingRecord(String key) {
        this.key = key;
        mappingValues = null;
    }

    public SortableMappingRecord(MappingRecord mappingValues) {
        if(mappingValues == null || StringUtils.isBlank(mappingValues.kategorieOderGruppe))
            throw new IllegalArgumentException("Invalid or missing mappingrecord.");
        this.mappingValues = mappingValues;
        this.key = mappingValues.kategorieOderGruppe;
    }

    @Override
    public String toString() {
        return "SortableMappingRecord Key(" +key + ") ---> " +super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SortableMappingRecord)) return false;
        SortableMappingRecord that = (SortableMappingRecord) o;
        return key.equalsIgnoreCase(that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
